﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace TetrisPairLibrary.ActiveObject
{
    public abstract class Figure
    {
        protected int rotatePosition = 0;
        protected BlocksGrid grid;
        public Point Position
        {
            get;
            set;
        }

        public BlocksGrid Grid
        {
            get => grid;
        }

        public BlockColor FigureColor
        {
            get;
            set;
        }

        public Figure ()
        {
            
        }

        public Figure(BlockColor color)
        {
            FigureColor = color;
        }

        public abstract FigureCollisionBox GetCollisionBox();

        public Point GetTopRight()
        {
            Point point = Position;
            for (int i = 0; i < grid.CountRow - 1; i++)
            {
                for (int j = grid.CountColumn - 1; j >= 0; j--)
                {
                    if (grid[i, j].Empty == false)
                    {
                        point = new Point(Position.X + j, Position.Y + i);
                        return point;
                    }
                }
            }
            return point;
        }

        public Point GetTopLeft()
        {
            Point point = Position;
            for (int i = 0; i < grid.CountRow - 1; i++)
            {
                for (int j = 0; j < grid.CountColumn - 1; j++)
                {
                    if (grid[i, j].Empty == false)
                    {
                        point = new Point(Position.X + j, Position.Y + i);
                        return point;
                    }
                }
            }
            return point;
        }

        /// <summary>
        /// Находит правый нижний угол
        /// </summary>
        /// <returns></returns>
        public Point GetBottomRight()
        {
            Point point = Position;
            for (int i = grid.CountRow - 1; i >= 0; i--)
            {
                for (int j = grid.CountColumn - 1; j >= 0; j--)
                {
                    if (grid[i,j].Empty == false)
                    {
                        point = new Point(Position.X + j, Position.Y + i);
                        return point;
                    }
                }
            }
            return point;
        }

        public Point GetBottomLeft()
        {
            Point point = Position;
            for (int i = grid.CountRow - 1; i >= 0; i--)
            {
                for (int j = 0; j < grid.CountColumn - 1; j++)
                {
                    if (grid[i, j].Empty == false)
                    {
                        point = new Point(Position.X + j, Position.Y + i);
                        return point;
                    }
                }
            }
            return point;
        }

        public abstract Point GetStartPosition(int widthField);
        public abstract void Rotate();
        public abstract void RotateRevers();
    }
}
