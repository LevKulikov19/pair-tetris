﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace TetrisPairLibrary.ActiveObject
{
    public class FigureCollisionBox
    {
        public Point TopLeftRealtive
        {
            get;
        }

        public Point TopRightRealtive
        {
            get;
        }

        public Point BottomLeftRealtive
        {
            get;
        }

        public Point BottomRightRealtive
        {
            get;
        }

        public FigureCollisionBox(Point topLeftRealtive, Point topRightRealtive, Point bottomLeftRealtive, Point bottomRightRealtive)
        {
            TopLeftRealtive = topLeftRealtive;
            TopRightRealtive = topRightRealtive;
            BottomLeftRealtive = bottomLeftRealtive;
            BottomRightRealtive = bottomRightRealtive;
        }

        public Point GetTopLeftAbsolute (Point point)
        {
            return new Point(TopLeftRealtive.X + point.X, TopLeftRealtive.Y + point.Y);
        }

        public Point GetTopRightAbsolute(Point point)
        {
            return new Point(TopRightRealtive.X + point.X, TopRightRealtive.Y + point.Y);
        }

        public Point GetBottomLeftAbsolute(Point point)
        {
            return new Point(BottomLeftRealtive.X + point.X, BottomLeftRealtive.Y + point.Y);
        }

        public Point GetBottomRightAbsolute(Point point)
        {
            return new Point(BottomRightRealtive.X + point.X, BottomRightRealtive.Y + point.Y);
        }
    }
}
