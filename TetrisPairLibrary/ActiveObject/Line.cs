﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace TetrisPairLibrary.ActiveObject
{
    public class Line : Figure
    {
        int Length;
        // rotatePosition = 0 Horizontal
        // rotatePosition = 1 Vertical
        public Line(int length = 4) : base(BlockColor.Random())
        {
            Length = length;
            InitGrid(length, FigureColor);
        }

        public Line(int length, BlockColor figureColor) : base(figureColor)
        {
            InitGrid(length, FigureColor);
        }

        private void InitGrid(int length, BlockColor figureColor)
        {
            if (length < 1) throw new ArgumentException("The length of the \"Line\" shape must be >1");
            grid = GetHorizontalGrid(length, figureColor);
        }

        private BlocksGrid GetHorizontalGrid (int length, BlockColor figureColor)
        {
            BlocksGrid gridBlock = new BlocksGrid(length, length);

            for (int i = 0; i < length; i++)
            {
                gridBlock[0, i] = new Block(figureColor);
            }
            return gridBlock;
        }

        private BlocksGrid GetVerticalGrid(int length, BlockColor figureColor)
        {
            BlocksGrid gridBlock = new BlocksGrid(length, length);

            for (int i = 0; i < length; i++)
            {
                gridBlock[i, 2] = new Block(figureColor);
            }
            return gridBlock;
        }

        public override Point GetStartPosition(int widthField)
        {
            Point point = new Point(0, 0);
            if (rotatePosition == 0)
            {
                int x = widthField / 2 - grid.CountRow/2;
                point = new Point(x, 0);
            }
            if (rotatePosition == 1)
            {
                int x = widthField / 2;
                point = new Point(x, 0);
            }
            return point;
        }

        public override FigureCollisionBox GetCollisionBox()
        {
            FigureCollisionBox collisionBox = null;
            if (rotatePosition == 0)
            {
                Point topLeft = new Point(0,0);
                Point topRight = new Point(grid.CountRow - 1, 0);
                Point bottomLeft = topLeft;
                Point bottomRight = topRight;
                collisionBox = new FigureCollisionBox(topLeft,topRight,bottomLeft,bottomRight);
            }
            if (rotatePosition == 1)
            {
                int x = 0;
                for (int i = 0; i < grid.CountRow; i++)
                {
                    if (grid[0,i].Empty == false)
                    {
                        x = i;
                        break;
                    }
                }
                Point topLeft = new Point(x, 0);
                Point topRight = topLeft;
                Point bottomLeft = new Point(x, grid.CountColumn - 1);
                Point bottomRight = bottomLeft;
                collisionBox = new FigureCollisionBox(topLeft, topRight, bottomLeft, bottomRight);
            }
            return collisionBox;
        }

        public override void Rotate()
        {
            if (rotatePosition == 0) rotatePosition = 1;
            else if (rotatePosition == 1) rotatePosition = 0;
            if (rotatePosition == 0) grid = GetHorizontalGrid(Length, FigureColor);
            if (rotatePosition == 1) grid = GetVerticalGrid(Length, FigureColor);
        }

        public override void RotateRevers()
        {
            if (rotatePosition == 0) rotatePosition = 1;
            else if (rotatePosition == 1) rotatePosition = 0;
            if (rotatePosition == 0) grid = GetHorizontalGrid(Length, FigureColor);
            if (rotatePosition == 1) grid = GetVerticalGrid(Length, FigureColor);
        }
    }
}
