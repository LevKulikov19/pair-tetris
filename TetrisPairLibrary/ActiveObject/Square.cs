﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace TetrisPairLibrary.ActiveObject
{
    public class Square : Figure
    {
        public Square(int height = 2, int width = 2) : base(BlockColor.Random())
        {
            InitGrid(height, width, FigureColor);
        }

        public Square(int height, int width, BlockColor figureColor) : base(figureColor)
        {
            InitGrid(height, width, FigureColor);
        }

        private void InitGrid(int height, int width, BlockColor figureColor)
        {
            grid = new BlocksGrid(height, width);
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    grid[i, j] = new Block(figureColor);
                }
            }
        }

        public override Point GetStartPosition(int widthField)
        {
            return new Point(widthField / 2, 0);
        }

        public override FigureCollisionBox GetCollisionBox()
        {
            Point topLeft = new Point(0,0);
            Point topRight = new Point(grid.CountRow-1, 0);
            Point bottomLeft = new Point(0, grid.CountColumn-1);
            Point bottomRight = new Point(grid.CountRow - 1, grid.CountColumn - 1);
            return new FigureCollisionBox(topLeft, topRight, bottomLeft, bottomRight);
        }

        public override void Rotate()
        {
            return;
        }

        public override void RotateRevers()
        {
            return;
        }
    }
}
