﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace TetrisPairLibrary.ActiveObject
{
    class T : Figure
    {
        public T() : base(BlockColor.Random())
        {
            InitGrid(FigureColor);
        }

        public T(BlockColor figureColor) : base(figureColor)
        {
            InitGrid(FigureColor);
        }

        private void InitGrid(BlockColor figureColor)
        {
            grid = GetGridRotate0(figureColor);
        }

        private BlocksGrid GetGridRotate0(BlockColor figureColor)
        {
            BlocksGrid gridBlock = new BlocksGrid(3, 3);

            gridBlock[0, 1] = new Block(figureColor);
            gridBlock[1, 0] = new Block(figureColor);
            gridBlock[1, 1] = new Block(figureColor);
            gridBlock[1, 2] = new Block(figureColor);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (gridBlock[i, j] == null)
                    {
                        Block blockEmpty = new Block(figureColor);
                        blockEmpty.Empty = true;
                        gridBlock[i, j] = blockEmpty;
                    }
                }
            }
            return gridBlock;
        }

        private BlocksGrid GetGridRotate1(BlockColor figureColor)
        {
            BlocksGrid gridBlock = new BlocksGrid(3, 3);

            gridBlock[0, 0] = new Block(figureColor);
            gridBlock[1, 0] = new Block(figureColor);
            gridBlock[2, 0] = new Block(figureColor);
            gridBlock[1, 1] = new Block(figureColor);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (gridBlock[i, j] == null)
                    {
                        Block blockEmpty = new Block(figureColor);
                        blockEmpty.Empty = true;
                        gridBlock[i, j] = blockEmpty;
                    }
                }
            }
            return gridBlock;
        }

        private BlocksGrid GetGridRotate2(BlockColor figureColor)
        {
            BlocksGrid gridBlock = new BlocksGrid(3, 3);

            gridBlock[0, 0] = new Block(figureColor);
            gridBlock[0, 1] = new Block(figureColor);
            gridBlock[0, 2] = new Block(figureColor);
            gridBlock[1, 1] = new Block(figureColor);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (gridBlock[i, j] == null)
                    {
                        Block blockEmpty = new Block(figureColor);
                        blockEmpty.Empty = true;
                        gridBlock[i, j] = blockEmpty;
                    }
                }
            }
            return gridBlock;
        }

        private BlocksGrid GetGridRotate3(BlockColor figureColor)
        {
            BlocksGrid gridBlock = new BlocksGrid(3, 3);

            gridBlock[0, 1] = new Block(figureColor);
            gridBlock[1, 0] = new Block(figureColor);
            gridBlock[1, 1] = new Block(figureColor);
            gridBlock[2, 1] = new Block(figureColor);


            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (gridBlock[i, j] == null)
                    {
                        Block blockEmpty = new Block(figureColor);
                        blockEmpty.Empty = true;
                        gridBlock[i, j] = blockEmpty;
                    }
                }
            }
            return gridBlock;
        }

        public override FigureCollisionBox GetCollisionBox()
        {
            FigureCollisionBox collisionBox = null;
            if (rotatePosition == 0)
            {
                Point topLeft = new Point(0, 0);
                Point topRight = new Point(2, 0);
                Point bottomLeft = new Point(0, 1);
                Point bottomRight = new Point(2, 1);
                collisionBox = new FigureCollisionBox(topLeft, topRight, bottomLeft, bottomRight);
            }
            if (rotatePosition == 1)
            {
                Point topLeft = new Point(0, 0);
                Point topRight = new Point(1, 0);
                Point bottomLeft = new Point(0, 2);
                Point bottomRight = new Point(1, 2);
                collisionBox = new FigureCollisionBox(topLeft, topRight, bottomLeft, bottomRight);
            }
            if (rotatePosition == 2)
            {
                Point topLeft = new Point(0, 0);
                Point topRight = new Point(2, 0);
                Point bottomLeft = new Point(0, 1);
                Point bottomRight = new Point(2, 1);
                collisionBox = new FigureCollisionBox(topLeft, topRight, bottomLeft, bottomRight);
            }
            if (rotatePosition == 3)
            {
                Point topLeft = new Point(0, 0);
                Point topRight = new Point(1, 0);
                Point bottomLeft = new Point(0, 2);
                Point bottomRight = new Point(1, 2);
                collisionBox = new FigureCollisionBox(topLeft, topRight, bottomLeft, bottomRight);
            }
            return collisionBox;
        }

        public override Point GetStartPosition(int widthField)
        {
            Point point = new Point(widthField / 2 - 1, 0);
            return point;
        }

        public override void Rotate()
        {
            if (rotatePosition == 0) rotatePosition = 1;
            else if (rotatePosition == 1) rotatePosition = 2;
            else if (rotatePosition == 2) rotatePosition = 3;
            else if (rotatePosition == 3) rotatePosition = 0;
            if (rotatePosition == 0) grid = GetGridRotate0(FigureColor);
            if (rotatePosition == 1) grid = GetGridRotate1(FigureColor);
            if (rotatePosition == 2) grid = GetGridRotate2(FigureColor);
            if (rotatePosition == 3) grid = GetGridRotate3(FigureColor);
        }

        public override void RotateRevers()
        {
            if (rotatePosition == 0) rotatePosition = 3;
            else if (rotatePosition == 1) rotatePosition = 0;
            else if (rotatePosition == 2) rotatePosition = 1;
            else if (rotatePosition == 3) rotatePosition = 2;
            if (rotatePosition == 0) grid = GetGridRotate0(FigureColor);
            if (rotatePosition == 1) grid = GetGridRotate1(FigureColor);
            if (rotatePosition == 2) grid = GetGridRotate2(FigureColor);
            if (rotatePosition == 3) grid = GetGridRotate3(FigureColor);
        }
    }
}
