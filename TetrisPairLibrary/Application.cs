﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TetrisPairLibrary
{
    public class Application
    {
        Game game;
        public Game Game
        {
            get => game;
        }

        public Settings Settings
        {
            get;
        }

        public Application()
        {
            Settings = new Settings();
            Settings.GameSettings.Changed += GameSettings_Changed;
            game = null;
        }

        private void GameSettings_Changed()
        {
            game = null;
        }

        public Game StartNewGame()
        {
            game = new Game(Settings.GameSettings);
            return game;
        }

        public void StopGame()
        {
            game = null;
        }
    }
}
