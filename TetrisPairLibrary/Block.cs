﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace TetrisPairLibrary
{
    public class Block
    {
        public bool Empty
        {
            get;
            set;
        }

        public BlockColor Color
        {
            get;
            set;
        }

        public Block (BlockColor blockColor)
        {
            Color = blockColor;
            Empty = false;
        }

        public Block Clone ()
        {
            Block block = new Block(Color);
            block.Empty = Empty;
            return block;
        }
    }
}
