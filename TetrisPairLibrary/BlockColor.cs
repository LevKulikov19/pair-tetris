﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using static TetrisPairLibrary.Block;

namespace TetrisPairLibrary
{
    public class BlockColor
    {
        private Color main;
        public Color Main
        {
            get => main;
            set
            {
                main = value;
            }
        }

        private Color side;
        public Color Side
        {
            get => side;
            set
            {
                side = value;
            }
        }

        private Color highlight;
        public Color Highlight
        {
            get => highlight;
            set
            {
                highlight = value;
            }
        }

        private Color shadow;
        public Color Shadow
        {
            get => shadow;
            set
            {
                shadow = value;
            }
        }

        /// <summary>
        /// 0-7 Normal 
        /// 100-107 Normal Light 
        /// 200-207 Normal Dark 
        /// 300-307 Low Saturation
        /// </summary>
        public enum BlockColorList
        {
            Black = 0,
            Blue = 1,
            Red = 2,
            Magenta = 3,
            Green = 4,
            Orange = 5,
            Cyan = 6,
            Yellow = 7,
            White = 8,
            BlackLight = 100,
            BlueLight = 101,
            RedLight = 102,
            MagentaLight = 103,
            GreenLight = 104,
            OrangeLight = 105,
            CyanLight = 106,
            YellowLight = 107,
            WhiteLight = 108,
            BlackDark = 200,
            BlueDark = 201,
            RedDark = 202,
            MagentaDark = 203,
            GreenDark = 204,
            OrangeDark = 205,
            CyanDark = 206,
            YellowDark = 207,
            WhiteDark = 208,
            BlackFlat = 300,
            BlueFlat = 301,
            RedFlat = 302,
            MagentaFlat = 303,
            GreenFlat = 304,
            OrangeFlat = 305,
            CyanFlat = 306,
            YellowFlat = 307,
            WhiteFlat = 308
        }
        public BlockColorList ColorValue
        {
            get;
        }

        public BlockColor (BlockColorList blockColor)
        {
            ColorValue = blockColor;
            SetBlockColor(blockColor);
        }

        public void SetBlockColor (BlockColorList blockColor)
        {
            switch (blockColor)
            {
                //Normal
                case BlockColorList.Black:
                    main = Color.FromArgb(48,48,48);
                    side = Color.FromArgb(39, 39, 39);
                    highlight = Color.FromArgb(148, 148, 148);
                    shadow = Color.FromArgb(25, 25, 25);
                    break;
                case BlockColorList.Blue:
                    main = Color.FromArgb(0, 0, 240);
                    side = Color.FromArgb(3, 0, 216);
                    highlight = Color.FromArgb(180, 178, 253);
                    shadow = Color.FromArgb(0, 1, 119);
                    break;
                case BlockColorList.Red:
                    main = Color.FromArgb(240, 0, 0);
                    side = Color.FromArgb(216, 0, 1);
                    highlight = Color.FromArgb(250, 178, 181);
                    shadow = Color.FromArgb(122, 0, 1);
                    break;
                case BlockColorList.Magenta:
                    main = Color.FromArgb(161, 0, 240);
                    side = Color.FromArgb(145, 0, 217);
                    highlight = Color.FromArgb(228, 178, 252);
                    shadow = Color.FromArgb(81, 0, 120);
                    break;
                case BlockColorList.Green:
                    main = Color.FromArgb(17, 197, 17);
                    side = Color.FromArgb(1, 177, 1);
                    highlight = Color.FromArgb(180, 250, 178);
                    shadow = Color.FromArgb(0, 122, 0); 
                    break;
                case BlockColorList.Orange:
                    main = Color.FromArgb(240, 160, 1);
                    side = Color.FromArgb(213, 145, 0);
                    highlight = Color.FromArgb(250, 228, 179);
                    shadow = Color.FromArgb(119, 81, 0);
                    break;
                case BlockColorList.Cyan:
                    main = Color.FromArgb(0, 240, 241);
                    side = Color.FromArgb(1, 216, 216);
                    highlight = Color.FromArgb(177, 253, 253);
                    shadow = Color.FromArgb(0, 120, 123);
                    break;
                case BlockColorList.Yellow:
                    main = Color.FromArgb(255, 255, 0);
                    side = Color.FromArgb(235, 234, 5);
                    highlight = Color.FromArgb(253, 251, 176);
                    shadow = Color.FromArgb(120, 120, 0); 
                    break;
                case BlockColorList.White:
                    main = Color.FromArgb(222, 222, 222);
                    side = Color.FromArgb(216, 216, 216);
                    highlight = Color.FromArgb(250, 250, 250);
                    shadow = Color.FromArgb(183, 183, 183);
                    break;
                //Normal Light
                case BlockColorList.BlackLight:
                    main = Color.FromArgb(74, 74, 74);
                    side = Color.FromArgb(64, 64, 64);
                    highlight = Color.FromArgb(173, 173, 173);
                    shadow = Color.FromArgb(51, 51, 51);
                    break;
                case BlockColorList.BlueLight:
                    main = Color.FromArgb(20, 20, 255);
                    side = Color.FromArgb(25, 22, 243);
                    highlight = Color.FromArgb(231, 230, 254);
                    shadow = Color.FromArgb(0, 1, 168);
                    break;
                case BlockColorList.RedLight:
                    main = Color.FromArgb(255, 23, 24);
                    side = Color.FromArgb(229, 26, 26);
                    highlight = Color.FromArgb(253, 226, 227);
                    shadow = Color.FromArgb(173, 0, 1);
                    break;
                case BlockColorList.MagentaLight:
                    main = Color.FromArgb(183, 36, 255);
                    side = Color.FromArgb(171, 27, 243);
                    highlight = Color.FromArgb(246, 230, 254);
                    shadow = Color.FromArgb(117, 0, 173);
                    break;
                case BlockColorList.GreenLight:
                    main = Color.FromArgb(37, 254, 37);
                    side = Color.FromArgb(27, 243, 27);
                    highlight = Color.FromArgb(227, 253, 226);
                    shadow = Color.FromArgb(0, 173, 0);
                    break;
                case BlockColorList.OrangeLight:
                    main = Color.FromArgb(254, 181, 37);
                    side = Color.FromArgb(243, 172, 22);
                    highlight = Color.FromArgb(253, 245, 226);
                    shadow = Color.FromArgb(168, 115, 0);
                    break;
                case BlockColorList.CyanLight:
                    main = Color.FromArgb(37, 254, 37);
                    side = Color.FromArgb(27, 243, 27);
                    highlight = Color.FromArgb(227, 253, 226);
                    shadow = Color.FromArgb(168, 115, 0);
                    break;
                case BlockColorList.YellowLight:
                    main = Color.FromArgb(255, 255, 51);
                    side = Color.FromArgb(224, 243, 47);
                    highlight = Color.FromArgb(227, 253, 226);
                    shadow = Color.FromArgb(173, 115, 0);
                    break;
                case BlockColorList.WhiteLight:
                    main = Color.FromArgb(242, 242, 242);
                    side = Color.FromArgb(247, 247, 247);
                    highlight = Color.FromArgb(255, 255, 255);
                    shadow = Color.FromArgb(209, 209, 209);
                    break;
                //Normal Dark
                case BlockColorList.BlackDark:
                    main = Color.FromArgb(23, 23, 23);
                    side = Color.FromArgb(13, 13, 13);
                    highlight = Color.FromArgb(97, 97, 97);
                    shadow = Color.FromArgb(0, 0, 0);
                    break;
                case BlockColorList.BlueDark:
                    main = Color.FromArgb(0, 0, 138);
                    side = Color.FromArgb(2, 0, 112);
                    highlight = Color.FromArgb(86, 81, 250);
                    shadow = Color.FromArgb(0, 1, 66);
                    break;
                case BlockColorList.RedDark:
                    main = Color.FromArgb(189, 0, 0);
                    side = Color.FromArgb(163, 0, 1);
                    highlight = Color.FromArgb(247, 130, 135);
                    shadow = Color.FromArgb(71, 0, 1);
                    break;
                case BlockColorList.MagentaDark:
                    main = Color.FromArgb(92, 0, 138);
                    side = Color.FromArgb(78, 0, 117);
                    highlight = Color.FromArgb(194, 81, 250);
                    shadow = Color.FromArgb(45, 0, 66);
                    break;
                case BlockColorList.GreenDark:
                    main = Color.FromArgb(1, 137, 1);
                    side = Color.FromArgb(1, 117, 1);
                    highlight = Color.FromArgb(87, 244, 83);
                    shadow = Color.FromArgb(0, 66, 0);
                    break;
                case BlockColorList.OrangeDark:
                    main = Color.FromArgb(137, 97, 1);
                    side = Color.FromArgb(112, 76, 0);
                    highlight = Color.FromArgb(244, 194, 83);
                    shadow = Color.FromArgb(66, 45, 0);
                    break;
                case BlockColorList.CyanDark:
                    main = Color.FromArgb(0, 137, 138);
                    side = Color.FromArgb(112, 76, 0);
                    highlight = Color.FromArgb(76, 250, 250);
                    shadow = Color.FromArgb(0, 65, 66);
                    break;
                case BlockColorList.YellowDark:
                    main = Color.FromArgb(215, 215, 0);
                    side = Color.FromArgb(197, 196, 0);
                    highlight = Color.FromArgb(250, 246, 76);
                    shadow = Color.FromArgb(93, 93, 0);
                    break;
                case BlockColorList.WhiteDark:
                    main = Color.FromArgb(171, 171, 171);
                    side = Color.FromArgb(166, 166, 166);
                    highlight = Color.FromArgb(199, 199, 199);
                    shadow = Color.FromArgb(133, 133, 133);
                    break;
                //Low Saturation
                case BlockColorList.BlackFlat:
                    main = Color.FromArgb(50, 50, 50);
                    side = Color.FromArgb(49, 49, 49);
                    highlight = Color.FromArgb(118, 118, 118);
                    shadow = Color.FromArgb(40, 40, 40);
                    break;
                case BlockColorList.BlueFlat:
                    main = Color.FromArgb(84, 84, 156);
                    side = Color.FromArgb(76, 76, 140);
                    highlight = Color.FromArgb(205, 205, 226);
                    shadow = Color.FromArgb(42, 42, 77);
                    break;
                case BlockColorList.RedFlat:
                    main = Color.FromArgb(178, 112, 112);
                    side = Color.FromArgb(169, 96, 96);
                    highlight = Color.FromArgb(243, 236, 237);
                    shadow = Color.FromArgb(113, 61, 61);
                    break;
                case BlockColorList.MagentaFlat:
                    main = Color.FromArgb(132, 84, 156);
                    side = Color.FromArgb(119, 76, 141);
                    highlight = Color.FromArgb(219, 206, 225);
                    shadow = Color.FromArgb(66, 42, 78);
                    break;
                case BlockColorList.GreenFlat:
                    main = Color.FromArgb(84, 157, 84);
                    side = Color.FromArgb(76, 141, 76);
                    highlight = Color.FromArgb(207, 221, 207);
                    shadow = Color.FromArgb(43, 79, 43);
                    break;
                case BlockColorList.OrangeFlat:
                    main = Color.FromArgb(157, 132, 84);
                    side = Color.FromArgb(138, 118, 75);
                    highlight = Color.FromArgb(222, 217, 207);
                    shadow = Color.FromArgb(77, 66, 42);
                    break;
                case BlockColorList.CyanFlat:
                    main = Color.FromArgb(84, 156, 157);
                    side = Color.FromArgb(138, 118, 75);
                    highlight = Color.FromArgb(205, 255, 225);
                    shadow = Color.FromArgb(43, 79, 80);
                    break;
                case BlockColorList.YellowFlat:
                    main = Color.FromArgb(217, 217, 104);
                    side = Color.FromArgb(205, 205, 100);
                    highlight = Color.FromArgb(225, 224, 204);
                    shadow = Color.FromArgb(101, 101, 49);
                    break;
                case BlockColorList.WhiteFlat:
                    main = Color.FromArgb(222, 222, 222);
                    side = Color.FromArgb(216, 216, 216);
                    highlight = Color.FromArgb(230, 230, 230);
                    shadow = Color.FromArgb(209, 209, 209);
                    break;
                default:
                    break;
            }
        }

        static public BlockColor Random ()
        {
            Random r = new Random();
            BlockColorList blockColor = (BlockColorList)r.Next(0, 8);
            return new BlockColor(blockColor);
        }
    }
}