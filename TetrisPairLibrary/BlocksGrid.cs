﻿using System.Drawing;

namespace TetrisPairLibrary
{
    public class BlocksGrid
    {
        private Block[,] grid;

        public int CountRow { get; } // колличество строк - y
        public int CountColumn { get; } // колличество столбцов - x

        public Block this[int r, int c]
        {
            get
            {
                return grid[r, c];
            }

            set
            {
                grid[r, c] = value;
            }
        }

        public Block[,] Grid { get => grid; }

        public BlocksGrid(int rows, int columns)
        {
            grid = new Block[rows, columns];
            CountRow = rows;
            CountColumn = columns;
            Clear();
        }

        public bool Inside(int r, int c)
        {
            return r >= 0 && r < CountRow && c >= 0 && c < CountColumn;
        }

        public bool Inside(Point p)
        {
            return Inside(p.Y, p.X);
        }

        public bool IsEmpty(int r, int c)
        {
            return Inside(r, c) && grid[r, c].Empty == true;
        }

        public bool IsEmpty(Point p)
        {
            return IsEmpty(p.Y, p.X);
        }

        public bool RowFull(int r)
        {
            for (int c = 0; c < CountColumn; c++)
            {
                if (grid[r, c].Empty == true)
                {
                    return false;
                }
            }
            return true;
        }

        public bool RowEmpty(int r)
        {
            for (int c = 0; c < CountColumn; c++)
            {
                if (grid[r, c].Empty == false)
                {
                    return false;
                }
            }
            return true;
        }

        public void RowClear(int r)
        {
            for (int c = 0; c < CountColumn; c++)
            {
                grid[r, c].Empty = true;
            }
        }

        public void Clear()
        {
            for (int i = 0; i < CountRow; i++)
            {
                for (int j = 0; j < CountColumn; j++)
                {
                    Block block = new Block(new BlockColor(BlockColor.BlockColorList.Black));
                    block.Empty = true;
                    grid[i, j] = block;
                }
            }
        }

        public int RowRemove(int row)
        {
            RowClear(row);
            for (int i = row-1; i >= 0; i--)
            {
                RowMoveDown(i);
            }
            int cleared = 0;



            for (int r = CountRow - 1; r >= 0; r--)
            {
                if (RowFull(r))
                {
                    RowClear(r);
                    cleared++;
                }
                else if (cleared > 0)
                {
                    RowMoveDown(r, cleared);
                }
            }
            return cleared;
        }


        public void RowMoveDown(int r, int numberRows)
        {
            for (int c = 0; c < CountColumn; c++)
            {
                grid[r + numberRows, c] = grid[r, c];
            }
        }

        public void RowMoveDown(int r)
        {
            for (int c = 0; c < CountColumn; c++)
            {
                grid[r + 1, c] = grid[r, c];
            }
        }

        public void RowMoveUp(int r)
        {
            for (int c = 0; c < CountColumn; c++)
            {
                grid[r - 1, c] = grid[r, c];

            }
        }

        public BlocksGrid Clone ()
        {
            BlocksGrid blocksGrid = new BlocksGrid(CountRow,CountColumn);
            for (int i = 0; i < CountRow; i++)
            {
                for (int j = 0; j < CountColumn; j++)
                {
                    blocksGrid[i, j] = grid[i, j].Clone();
                }
            }
            return blocksGrid;
        }

    }
}
