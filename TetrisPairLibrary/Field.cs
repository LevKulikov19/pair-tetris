﻿using System;
using System.Collections.Generic;
using System.Drawing;
using TetrisPairLibrary.ActiveObject;
using System.Text;

namespace TetrisPairLibrary
{
    public delegate void FieldEvent(Field sender);

    public class Field
    {
        /// <summary>
        /// 1 параметр - строка, 2 параметр - столбец
        /// </summary>
        private BlocksGrid GridStatic;
        private BlocksGrid GridMotion;
        private Figure ActiveObject;

        public event FieldEvent BlockDropped;
        public event FieldEvent FieldFull;
        public event FieldEvent Changed;
        public event FieldEvent RemoveLastLineComplete;

        public int CountRow { get; } // колличество строк - y
        public int CountColumn { get; } // колличество столбцов - x
        public Block this[int r, int c]
        {
            get
            {
                Block[,] Grid = GridStatic.Clone().Grid;
                for (int i = 0; i < CountRow; i++)
                {
                    for (int j = 0; j < CountColumn; j++)
                    {
                        if (GridMotion[i,j].Empty == false)
                        {
                            Grid[i, j] = GridMotion[i, j];
                        }
                    }
                }
                return Grid[r, c];
            }
        }

        public Field(int rows, int columns)
        {
            CountRow = rows;
            CountColumn = columns;
            GridStatic = new BlocksGrid(rows, columns);
            GridMotion = new BlocksGrid(rows, columns);
        }

        private int[] LinesComplete (BlocksGrid grid)
        {
            List<int> lines = new List<int>();
            for (int i = 0; i < CountRow; i++)
            {
                int countCompleted = 0;
                for (int j = 0; j < CountColumn; j++)
                {
                    if (grid[i,j].Empty == false)
                    {
                        countCompleted++;
                    }
                }
                if (countCompleted == CountColumn)
                {
                    lines.Add(i);
                }
            }
            return lines.ToArray();
        }

        private void LineCompleteRemove (BlocksGrid grid)
        {
            int[] lineComplete = LinesComplete(grid);
            foreach (int item in lineComplete)
            {
                grid.RowRemove(item);
            }
            for (int i = 0; i < lineComplete.Length; i++)
            {
                RemoveLastLineComplete(this);
            }
        }

        public bool AddActiveFigure(Figure figure)
        {
            if (ActiveObject != null) return false;
            ActiveObject = figure;
            ActiveObject.Position = ActiveObject.GetStartPosition(CountColumn);
            if (isCollisionBlock(GridStatic, ActiveObject, ActiveObject.Position) || isCollisionBlock(GridStatic, ActiveObject, new Point(ActiveObject.Position.X, ActiveObject.Position.Y+1)))
            {
                ActiveObjectOnGridMotion(ActiveObject, GridMotion);
                FieldFull(this);
                return false;
            }
            ActiveObjectOnGridMotion(ActiveObject, GridMotion);
            return true;
        }

        public bool MoveDownActiveFigure()
        {
            if (ActiveObject == null) return false;
            Point newPosition = new Point(ActiveObject.Position.X, ActiveObject.Position.Y + 1);
            if (isCollisionBoxBottomEdge (ActiveObject, newPosition) || isCollisionBlock(GridStatic, ActiveObject, newPosition))
            {
                CopyGridMotionToGridStatic(GridMotion,GridStatic);
                LineCompleteRemove(GridStatic);
                GridMotion.Clear();
                ActiveObject = null;
                BlockDropped(this);
            }
            else
            {
                if (isCollisionBoxEdge(ActiveObject, newPosition)) return false;
                ActiveObject.Position = newPosition;
                GridMotion = new BlocksGrid(CountRow,CountColumn);
                ActiveObjectOnGridMotion(ActiveObject, GridMotion);
            }
            Changed(this);
            return true;
        }

        public bool MoveLeftActiveFigure()
        {
            if (ActiveObject == null) return false;
            Point newPosition = new Point(ActiveObject.Position.X - 1, ActiveObject.Position.Y);
            if (isCollisionBoxEdge(ActiveObject, newPosition) || isCollisionBlock(GridStatic, ActiveObject, newPosition)) return false;
            ActiveObject.Position = newPosition;
            GridMotion = new BlocksGrid(CountRow, CountColumn);
            ActiveObjectOnGridMotion(ActiveObject, GridMotion);
            Changed(this);
            return true;
        }

        public bool MoveRightActiveFigure()
        {
            if (ActiveObject == null) return false;
            Point newPosition = new Point(ActiveObject.Position.X + 1, ActiveObject.Position.Y);
            if (isCollisionBoxEdge(ActiveObject, newPosition) || isCollisionBlock(GridStatic, ActiveObject, newPosition)) return false;
            ActiveObject.Position = newPosition;
            GridMotion = new BlocksGrid(CountRow, CountColumn);
            ActiveObjectOnGridMotion(ActiveObject, GridMotion);
            Changed(this);
            return true;
        }

        public bool MoveTopActiveFigure()
        {
            if (ActiveObject == null) return false;
            Point newPosition = new Point(ActiveObject.Position.X, ActiveObject.Position.Y - 1);
            if (isCollisionBoxEdge(ActiveObject, newPosition) || isCollisionBlock(GridStatic, ActiveObject, newPosition)) return false;
            ActiveObject.Position = newPosition;
            GridMotion = new BlocksGrid(CountRow, CountColumn);
            ActiveObjectOnGridMotion(ActiveObject, GridMotion);
            Changed(this);
            return true;
        }

        private bool isCollisionBoxEdge (Figure activeObject, Point newPosition)
        {
            if (isCollisionBoxBottomEdge(activeObject, newPosition)) return true;
            if (isCollisionBoxLeftEdge(activeObject, newPosition)) return true;
            if (isCollisionBoxRightEdge(activeObject, newPosition)) return true;
            if (isCollisionBoxTopEdge(activeObject, newPosition)) return true;
            return false;
        }

        private bool isCollisionBoxBottomEdge(Figure activeObject, Point newPosition)
        {
            Point p = activeObject.GetCollisionBox().GetBottomLeftAbsolute(newPosition);
            if (p.Y >= CountRow) return true;
            return false;
        }

        private bool isCollisionBoxLeftEdge(Figure activeObject, Point newPosition)
        {
            Point p = activeObject.GetCollisionBox().GetBottomLeftAbsolute(newPosition);
            if (p.X < 0) return true;
            return false;
        }

        private bool isCollisionBoxRightEdge(Figure activeObject, Point newPosition)
        {
            Point p = activeObject.GetCollisionBox().GetBottomRightAbsolute(newPosition);
            if (p.X >= CountColumn) return true;
            return false;
        }

        private bool isCollisionBoxTopEdge(Figure activeObject, Point newPosition)
        {
            Point p = activeObject.GetCollisionBox().GetTopLeftAbsolute(newPosition);
            if (p.Y < 0) return true;
            return false;
        }

        private bool isCollisionBlock(BlocksGrid gridStatic, Figure activeObject, Point newPosition)
        {
            BlocksGrid tempGridMotion = new BlocksGrid(gridStatic.CountRow, gridStatic.CountColumn);
            Point oldPosition = activeObject.Position;
            activeObject.Position = newPosition;
            ActiveObjectOnGridMotion(activeObject, tempGridMotion);
            for (int i = 0; i < gridStatic.CountRow; i++)
            {
                for (int j = 0; j < gridStatic.CountColumn; j++)
                {
                    if ((tempGridMotion[i, j].Empty == false) && (gridStatic[i, j].Empty == false))
                    {
                        activeObject.Position = oldPosition;
                        return true;
                    }
                }
            }
            activeObject.Position = oldPosition;
            return false;
        }


        private void ActiveObjectOnGridMotion(Figure activeObject, BlocksGrid grid)
        {
            for (int i = 0; i < grid.CountRow; i++)
            {
                for (int j = 0; j < grid.CountColumn; j++)
                {
                    Block block = new Block(new BlockColor(BlockColor.BlockColorList.Black));
                    block.Empty = true;
                    grid[i, j] = block;
                }
            }

            for (int i = 0; i < activeObject.Grid.CountRow; i++)
            {
                for (int j = 0; j < activeObject.Grid.CountColumn; j++)
                {
                    int row = activeObject.Position.Y + i;
                    int col = activeObject.Position.X + j;
                    if (ActiveObject.Grid[i, j].Empty == false)
                    {
                        grid[row, col] = ActiveObject.Grid[i, j];
                    }
                }
            }
        }

        private void CopyGridMotionToGridStatic(BlocksGrid gridMotion, BlocksGrid gridStatic)
        {
            for (int i = 0; i < CountRow; i++)
            {
                for (int j = 0; j < CountColumn; j++)
                {
                    if (gridMotion[i,j].Empty == false)
                    {
                        gridStatic[i, j] = gridMotion[i, j];
                    }
                }
            }
        }

        public void DeleteActiveObject ()
        {
            ActiveObject = null;
            GridMotion.Clear();
        }

        public void AddLastLineWithRandomSpaces()
        {
            for (int i = 1; i < CountRow; i++)
            {
                GridStatic.RowMoveUp(i);
            }
            for (int i = 0; i < CountColumn; i++)
            {
                Block block = new Block(BlockColor.Random());
                Random r = new Random();
                if (r.Next(0, 5) == 0) block.Empty = true;
                GridStatic[CountRow - 1, i] = block;
            }

            int count = 0;
            for (int i = 0; i < CountColumn; i++)
            {
                if (GridStatic[CountRow - 1, i].Empty == false) count++;
            }
            if (count == CountColumn)
            {
                GridStatic[CountRow - 1, CountColumn / 2].Empty = true;
            }
            
            Changed(this);
        }

        public void RotateActiveObject ()
        {
            ActiveObject.Rotate();
            if (isCollisionBoxEdge(ActiveObject, ActiveObject.Position) || isCollisionBlock(GridStatic, ActiveObject, ActiveObject.Position))
            {
                ActiveObject.RotateRevers();
            }
            GridMotion = new BlocksGrid(CountRow, CountColumn);
            ActiveObjectOnGridMotion(ActiveObject, GridMotion);
        }
    }
}
