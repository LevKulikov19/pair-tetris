﻿using System;
using System.Collections.Generic;
using System.Text;
using TetrisPairLibrary.ActiveObject;
using System.Windows.Input;
using System.Windows.Forms;

namespace TetrisPairLibrary
{
    public delegate void GameEvent();
    public class Game
    {
        public event GameEvent FieldUpdate;
        public event GameEvent Win;
        public event GameEvent PointsUpdate;

        Timer timer;

        GameSettings gameSettings;
        GameRules gameRules;
        Field[] fields;

        public int PlayerWin = 0;
        int[] points;
        public int[] Points
        {
            get => points;
        }
        Figure nextFigure;
        public Figure NextFigure
        {
            get => nextFigure;
        }

        public Field[] Fields
        {
            get => fields;
        }

        private bool isPause;
        public bool IsPause
        {
            get => isPause;
        }
        private bool isStop;
        public bool IsStop
        {
            get => isStop;
        }
        public Game (GameSettings gameSettings)
        {

            isPause = false;
            isStop = false;
            this.gameSettings = gameSettings;

            timer = new Timer();
            timer.Enabled = true;
            timer.Tick += Timer_Tick;

            GameSettinsSet();
            points = new int[0];
            fields = new Field[0];
            addField(15, 8);
            addField(15, 8);
            ConnectEventFields(fields);
            foreach (Field item in fields)
            {
                AddObjectFields(item);
            }

        }

        private void GameSettinsSet()
        {
            switch (gameSettings.DifficultyLevels)
            {
                case GameRules.DifficultyLevels.Easy:
                    gameRules = new GameRulesEasy();
                    break;
                case GameRules.DifficultyLevels.Medium:
                    gameRules = new GameRulesMedium();
                    break;
                case GameRules.DifficultyLevels.Hard:
                    gameRules = new GameRulesHard();
                    break;
                default:
                    break;
            }
            timer.Interval = gameRules.DropInterval;
        }

        private void ConnectEventFields(Field[] fieldsList)
        {
            foreach (var item in fieldsList)
            {
                item.BlockDropped += AddObjectFields;
                item.FieldFull += Game_FieldFull;
                item.Changed += Game_Changed;
                item.RemoveLastLineComplete += Game_RemoveLastLineComplete;
            }
        }

        private void DisconnectEventFields(Field[] fieldsList)
        {
            foreach (var item in fieldsList)
            {
                item.BlockDropped -= AddObjectFields;
                item.FieldFull -= Game_FieldFull;
                item.Changed -= Game_Changed;
                item.RemoveLastLineComplete -= Game_RemoveLastLineComplete;
            }
        }

        private void Game_RemoveLastLineComplete(Field sender)
        {
            foreach (Field item in fields)
            {
                if (item != sender)
                {
                    item.DeleteActiveObject();
                    item.AddLastLineWithRandomSpaces();
                    AddObjectFields(item);
                }
            }
            for (int i = 0; i < fields.Length; i++)
            {
                if(fields[i] == sender)
                {
                    points[i]++;
                    PointsUpdate();
                }
            }
        }

        private void Game_Changed(Field sender)
        {
            FieldUpdate();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            foreach (Field item in fields)
            {
                item.MoveDownActiveFigure();
            }
        }

        private void Game_FieldFull(Field sender)
        {
            Pause();
            for (int i = 0; i < fields.Length; i++)
            {
                if (fields[i] != sender) PlayerWin = i+1;
            }
            Win();
        }

        void addField (int rows, int columns)
        {
            Array.Resize<Field>(ref fields, fields.Length + 1);
            fields[fields.Length-1] = new Field(rows, columns);
            Array.Resize<int>(ref points, points.Length + 1);
            points[points.Length - 1] = 0;
        }

        public void GameForm_KeyDown(Keys key)
        {
            if (isStop) return;
            if (Keys.S == key) fields[0].MoveDownActiveFigure();
            if (Keys.A == key) fields[0].MoveLeftActiveFigure();
            if (Keys.D == key) fields[0].MoveRightActiveFigure();
            //if (Keys.W == key) fields[0].MoveTopActiveFigure();
            if (Keys.R == key) fields[0].RotateActiveObject();

            if (Keys.K == key) fields[1].MoveDownActiveFigure();
            if (Keys.J == key) fields[1].MoveLeftActiveFigure();
            if (Keys.L == key) fields[1].MoveRightActiveFigure();
            //if (Keys.I == key) fields[1].MoveTopActiveFigure();
            if (Keys.P == key) fields[1].RotateActiveObject();
        }

        private void AddObjectFields (Field sender)
        {
            Random random = new Random();
            if (nextFigure == null) nextFigure = RandomFigureByType(gameRules.ValidFigures[random.Next(0, gameRules.ValidFigures.Length-1)]);
            if (!sender.AddActiveFigure(nextFigure)) Stop();
            nextFigure = RandomFigureByType(gameRules.ValidFigures[random.Next(0, gameRules.ValidFigures.Length)]);
        }

        private Figure RandomFigureByType (Figure type)
        {
            if (type is Line) return new Line();
            if (type is Square) return new Square();
            if (type is L) return new L();
            if (type is LFlip) return new LFlip();
            if (type is Z) return new Z();
            if (type is ZFlip) return new ZFlip();
            if (type is T) return new T();
            return new Line();
        }

        public void Pause ()
        {
            DisconnectEventFields(fields);
            timer.Enabled = false;
            isPause = true;
        }

        public void Resume ()
        {
            ConnectEventFields(fields);
            timer.Enabled = true;
            isPause = false;
        }

        public void Stop()
        {
            DisconnectEventFields(fields);
            timer.Enabled = false;
            isStop = true;
            foreach (var item in fields)
            {
                item.BlockDropped += Empty;
                item.FieldFull += Empty;
                item.Changed += Empty;
                item.RemoveLastLineComplete += Empty;
            }
        }

        private void Empty(Field sender)
        {

        }
    }
}
