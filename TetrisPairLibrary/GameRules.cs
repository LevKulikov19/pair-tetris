﻿using System;
using System.Collections.Generic;
using System.Text;
using TetrisPairLibrary.ActiveObject;

namespace TetrisPairLibrary
{
    public abstract class GameRules
    {
        public enum DifficultyLevels
        {
            Easy = 0,
            Medium = 1,
            Hard = 2
        }

        public int DropInterval
        {
            get;
        }

        /// <summary>
        /// value in milliseconds
        /// </summary>
        public Figure[] ValidFigures
        {
            get;
        }

        protected GameRules (int dropInterval, Figure[] validFigures)
        {
            DropInterval = dropInterval;
            ValidFigures = validFigures;
        }
    }
}
