﻿using System;
using System.Collections.Generic;
using System.Text;
using TetrisPairLibrary.ActiveObject;

namespace TetrisPairLibrary
{
    public class GameRulesEasy : GameRules
    {
        static readonly int Interval = 1000;
        static readonly new Figure[] ValidFigures = {
            new Line(),
            new Square(),
            new T()
        };

        public GameRulesEasy() : base(Interval, ValidFigures)
        {

        }
    }
}
