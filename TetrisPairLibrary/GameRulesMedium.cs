﻿using System;
using System.Collections.Generic;
using System.Text;
using TetrisPairLibrary.ActiveObject;

namespace TetrisPairLibrary
{
    public class GameRulesMedium : GameRules
    {
        static readonly int Interval = 1000;
        static readonly new Figure[] ValidFigures = {
            new Line(),
            new Square(),
            new L(),
            new LFlip(),
            new Z(),
            new ZFlip(),
            new T()
        };

        public GameRulesMedium() : base(Interval, ValidFigures)
        {

        }
    }
}
