﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TetrisPairLibrary
{
    public delegate void GameSettingsEvent();
    public class GameSettings
    {
        public event GameSettingsEvent Changed;
        private GameRules.DifficultyLevels difficultyLevels;
        public GameRules.DifficultyLevels DifficultyLevels
        {
            get => difficultyLevels;
            set
            {
                if (Changed != null) Changed();
                difficultyLevels = value;
            }
        }

        public GameSettings()
        {
            DifficultyLevels = GameRules.DifficultyLevels.Easy;
        }
    }
}
