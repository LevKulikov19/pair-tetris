﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TetrisPairLibrary
{
    public class Settings
    {
        public GameSettings GameSettings
        {
            get;
        }

        public Settings()
        {
            GameSettings = new GameSettings();
        }
    }
}
