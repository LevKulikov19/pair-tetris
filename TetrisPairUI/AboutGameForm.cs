﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Text;
using System.Windows.Forms;

namespace TetrisPairUI
{
    public partial class AboutGameForm : Form
    {
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont,
        IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);
        private PrivateFontCollection fonts = new PrivateFontCollection();
        Font pixelFont;


        public AboutGameForm()
        {
            InitializeComponent();

            byte[] fontData = Properties.Resources.opirus_opik;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, Properties.Resources.opirus_opik.Length);
            AddFontMemResourceEx(fontPtr, (uint)Properties.Resources.opirus_opik.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);
            pixelFont = new Font(fonts.Families[0], 16.0F);

            this.buttonBlockMenu.Font = new Font(pixelFont.FontFamily, 20);
            this.labelText.Font = new Font(pixelFont.FontFamily, 30);
        }

        private void buttonBlockMenu_Click(object sender, EventArgs e)
        {
            ((Меню)this.MdiParent).ShowMenuForm();
        }
    }
}
