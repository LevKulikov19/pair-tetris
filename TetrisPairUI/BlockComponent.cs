﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using TetrisPairLibrary;

namespace TetrisPairUI
{
    public partial class BlockComponent : UserControl
    {
        public BlockColor.BlockColorList Color
        {
            get;
            set;
        }

        public int Ledge
        {
            get;
            set;
        }

        private BufferedGraphics GraphicsBlock;
        public void SetGraphics(BufferedGraphics graphics)
        {
            GraphicsBlock = graphics;
        }

        public BlockComponent()
        {
            InitializeComponent();
            Color = BlockColor.BlockColorList.Black;
            Ledge = 5;
            
        }

        private void pictureBlock_Paint(object sender, PaintEventArgs e)
        {
            //Redrowing();
        }

        public void Redrowing()
        {
            if (GraphicsBlock == null)
            {
                Graphics PictureBlockGraphics = this.CreateGraphics();
                PictureBlockGraphics.SmoothingMode = SmoothingMode.AntiAlias;
                BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
                BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(PictureBlockGraphics, this.DisplayRectangle);
                bufferedGraphics.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                GraphicsBlock = bufferedGraphics;
            }
            DrowBlock(new Point(0, 0), new BlockColor(Color), Size, GraphicsBlock.Graphics, Ledge);
            GraphicsBlock.Render(this.CreateGraphics());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position">Координаты вверней левой точки</param>
        /// <param name="blockColor"></param>
        public static void DrowBlock(Point position, BlockColor blockColor, Size BlockSize, Graphics graphics, int ledge = 5)
        {
            

            Brush brushMain = new SolidBrush(blockColor.Main);
            Point[] pointsMain = new Point[4];
            pointsMain[0] = new Point(position.X + ledge, position.Y + ledge);
            pointsMain[1] = new Point(position.X + BlockSize.Width - ledge, position.Y + ledge);
            pointsMain[2] = new Point(position.X + BlockSize.Width - ledge, position.Y + BlockSize.Height - ledge);
            pointsMain[3] = new Point(position.X + ledge, position.Y + BlockSize.Height - ledge);
            graphics.FillPolygon(brushMain, pointsMain);

            Brush brushSide = new SolidBrush(blockColor.Side);
            Point[] pointsSideLeft = new Point[4];
            pointsSideLeft[0] = new Point(position.X, position.Y);
            pointsSideLeft[1] = new Point(position.X + ledge, position.Y + ledge);
            pointsSideLeft[2] = new Point(position.X + ledge, position.Y + BlockSize.Height - ledge);
            pointsSideLeft[3] = new Point(position.X, position.Y + BlockSize.Height);
            graphics.FillPolygon(brushSide, pointsSideLeft);

            Point[] pointsSideRight = new Point[4];
            pointsSideRight[0] = new Point(position.X + BlockSize.Width, position.Y);
            pointsSideRight[1] = new Point(position.X + BlockSize.Width, position.Y + BlockSize.Height);
            pointsSideRight[2] = new Point(position.X + BlockSize.Width - ledge, position.Y + BlockSize.Height - ledge);
            pointsSideRight[3] = new Point(position.X + BlockSize.Width - ledge, position.Y + ledge);
            graphics.FillPolygon(brushSide, pointsSideRight);

            Brush brushTop = new SolidBrush(blockColor.Highlight);
            Point[] pointsTop = new Point[4];
            pointsTop[0] = new Point(position.X, position.Y);
            pointsTop[1] = new Point(position.X + BlockSize.Width, position.Y);
            pointsTop[2] = new Point(position.X + BlockSize.Width - ledge, position.Y + ledge);
            pointsTop[3] = new Point(position.X + ledge, position.Y + ledge);
            graphics.FillPolygon(brushTop, pointsTop);

            Brush brushDown = new SolidBrush(blockColor.Shadow);
            Point[] pointsDown = new Point[4];
            pointsDown[0] = new Point(position.X, position.Y + BlockSize.Height);
            pointsDown[1] = new Point(position.X + ledge, position.Y + BlockSize.Height - ledge);
            pointsDown[2] = new Point(position.X + BlockSize.Width - ledge, position.Y + BlockSize.Height - ledge);
            pointsDown[3] = new Point(position.X + BlockSize.Width, position.Y + BlockSize.Height);
            graphics.FillPolygon(brushDown, pointsDown);
        }

        private void BlockComponent_Paint(object sender, PaintEventArgs e)
        {
            Redrowing();
        }

        private void BlockComponent_Load(object sender, EventArgs e)
        {

        }
    }
}
