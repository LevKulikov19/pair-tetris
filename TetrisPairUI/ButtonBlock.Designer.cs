﻿
namespace TetrisPairUI
{
    partial class ButtonBlock
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ButtonBlock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ButtonBlock";
            this.Size = new System.Drawing.Size(514, 140);
            this.Load += new System.EventHandler(this.ButtonBlock_Load);
            this.EnabledChanged += new System.EventHandler(this.ButtonBlock_EnabledChanged);
            this.Click += new System.EventHandler(this.ButtonBlock_Click);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ButtonBlock_Paint);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ButtonBlock_MouseClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonBlock_MouseDown);
            this.MouseLeave += new System.EventHandler(this.ButtonBlock_MouseLeave);
            this.MouseHover += new System.EventHandler(this.ButtonBlock_MouseHover);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonBlock_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
