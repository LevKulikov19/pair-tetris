﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using TetrisPairLibrary;

namespace TetrisPairUI
{
    public partial class ButtonBlock : UserControl
    {
        public Point PositionText
        {
            get;
            set;
        }

        enum State
        {
            normal = 0,
            hover = 1,
            press = 2,
            disabled = 3
        }

        private State state;
        BufferedGraphics graphics;

        public BlockColor.BlockColorList ColorButton
        {
            get;
            set;
        }

        public int Ledge
        {
            get;
            set;
        }

        public String TextButton
        {
            get;
            set;
        }

        private Color ButtonTextColor;

        public ButtonBlock()
        {
            InitializeComponent();
            state = State.normal;
            ColorButton = BlockColor.BlockColorList.Black;
            Ledge = 5;
            TextButton = String.Empty;
            PositionText = new Point(-1, -1);
            Redrowing();
        }

        public void SetGraphics (BufferedGraphics bufferedGraphics)
        {
            graphics = bufferedGraphics;
        }

        private void ButtonBlock_Paint(object sender, PaintEventArgs e)
        {
            Redrowing();
        }

        private void ButtonBlock_Load(object sender, EventArgs e)
        {
            //Redrowing();
        }

        private void ButtonBlock_MouseHover(object sender, EventArgs e)
        {
            if (Enabled) state = State.hover;
            Redrowing();
        }

        private void ButtonBlock_Click(object sender, EventArgs e)
        {

        }

        private void ButtonBlock_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void ButtonBlock_MouseDown(object sender, MouseEventArgs e)
        {
            if (Enabled) state = State.press;
            Redrowing();
        }

        private void ButtonBlock_MouseLeave(object sender, EventArgs e)
        {
            if (Enabled) state = State.normal;
            Redrowing();
        }

        private void ButtonBlock_MouseUp(object sender, MouseEventArgs e)
        {
            if (Enabled) state = State.hover;
            Redrowing();
        }

        private void Redrowing ()
        {
            if (graphics == null)
            {
                Graphics PictureBlockGraphics = this.CreateGraphics();
                PictureBlockGraphics.SmoothingMode = SmoothingMode.AntiAlias;
                BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
                BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(PictureBlockGraphics, this.DisplayRectangle);
                bufferedGraphics.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics = bufferedGraphics;
            }
            BlockComponent.DrowBlock(new Point(0, 0), BlockColorState(new BlockColor(ColorButton), state), Size, graphics.Graphics, Ledge);
            ButtonTextColorUpdate();
            Brush brush = new SolidBrush(ButtonTextColor);
            int x, y;
            if (PositionText.X == -1 && PositionText.Y == -1)
            {
                x = (Size.Width - (int)(((int)Font.Size / 1.3)) * TextButton.Length) / 2;
                y = (Size.Height - (int)Font.Size) / 6;
            }
            else
            {
                x = PositionText.X;
                y = PositionText.Y;
            }
            PointF point = new PointF(x,y);
            graphics.Graphics.DrawString(TextButton, Font, brush, point);
            graphics.Render(this.CreateGraphics());
        }

        private BlockColor BlockColorState (BlockColor blockColor, State state)
        {
            BlockColor blockColorState;
            int colorNum = (int)blockColor.ColorValue;
            switch (state)
            {
                case State.normal:
                    if (colorNum >= 100) colorNum = colorNum % 100;
                    break;
                case State.hover:
                    if (colorNum < 100) colorNum = colorNum + 100;
                    if (colorNum >= 200) colorNum = colorNum % 100 + 100;
                    break;
                case State.press:
                    if ((colorNum >= 300) || (colorNum < 200)) colorNum = colorNum % 100 + 200;
                    break;
                case State.disabled:
                    if ((colorNum >= 400) || (colorNum < 300)) colorNum = colorNum % 100 + 300;
                    break;
                default:
                    break;
            }
            blockColorState = new BlockColor((BlockColor.BlockColorList)colorNum);

            return blockColorState;
        }

        private void ButtonBlock_EnabledChanged(object sender, EventArgs e)
        {
            if (Enabled) state = State.normal;
            else state = State.disabled;
        }

        private void ButtonTextColorUpdate()
        {
            if (Enabled) ButtonTextColor = ForeColor;
            else ButtonTextColor = Color.FromArgb(ForeColor.R / 2, ForeColor.G / 2, ForeColor.B / 2);
        }

    }
}
