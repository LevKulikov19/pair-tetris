﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using TetrisPairLibrary;

namespace TetrisPairUI
{
    public partial class FieldComponent : UserControl
    {
        public static readonly Size BlockSize = new Size(40,40);
        Graphics FieldGraphics;
        BufferedGraphics bufferedGraphics;
        Field field;

        private int border;

        public bool LowSaturaionBlock
        {
            get;
            set;
        }
        
        public FieldComponent(Field field)
        {
            this.field = field;
            InitializeComponent();
            border = 6;
            Size = new Size(field.CountColumn * BlockSize.Width + border*2, field.CountRow * BlockSize.Height + border*2);
        }

        private void FieldPanel_Paint(object sender, PaintEventArgs e)
        {
            FieldGraphics = FieldPanel.CreateGraphics();
            FieldGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            bufferedGraphics = bufferedGraphicsContext.Allocate(FieldGraphics, this.DisplayRectangle);
            Redrowing();
        }

        public void Redrowing()
        {
            int stroke = 5;
            bufferedGraphics.Graphics.Clear(Color.Black);
            for (int i = 0; i < field.CountRow; i++)
            {
                for (int j = 0; j < field.CountColumn; j++)
                {
                    Point position = new Point(BlockSize.Width*j + border, BlockSize.Height*i + border);
                    if (field[i, j].Empty == false)
                    {
                        BlockComponent.DrowBlock(position, ProcessingBlockColor(field[i, j].Color), BlockSize, bufferedGraphics.Graphics);
                    }
                }
            }
            Pen pen = new Pen(Color.Blue, stroke);
            bufferedGraphics.Graphics.DrawRectangle(pen, stroke / 2, stroke / 2, FieldPanel.Width - stroke, FieldPanel.Height - stroke);
            bufferedGraphics.Render(FieldPanel.CreateGraphics());
        }

        BlockColor ProcessingBlockColor(BlockColor blockColor)
        {
            BlockColor.BlockColorList blockColorList = blockColor.ColorValue;
            if (LowSaturaionBlock)
            {
                blockColorList = (BlockColor.BlockColorList)((int)blockColor.ColorValue % 10 + 300);
            }
            else
            {
                blockColorList = (BlockColor.BlockColorList)((int)blockColor.ColorValue % 10);
            }
            BlockColor color = new BlockColor(blockColorList);
            return color;
        }
    }
}
