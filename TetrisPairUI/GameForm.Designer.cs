﻿
namespace TetrisPairUI
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Wrapper = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelKeyP = new System.Windows.Forms.Label();
            this.labelKeyL = new System.Windows.Forms.Label();
            this.labelKeyK = new System.Windows.Forms.Label();
            this.labelKeyJ = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonBlockPause = new TetrisPairUI.ButtonBlock();
            this.labelPlayer2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonBlockMenu = new TetrisPairUI.ButtonBlock();
            this.labelPlayer1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureNextFigure = new System.Windows.Forms.PictureBox();
            this.labelNextFigure = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelKeyR = new System.Windows.Forms.Label();
            this.labelKeyD = new System.Windows.Forms.Label();
            this.labelKeyS = new System.Windows.Forms.Label();
            this.labelKeyA = new System.Windows.Forms.Label();
            this.Wrapper.SuspendLayout();
            this.panel2.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureNextFigure)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Wrapper
            // 
            this.Wrapper.BackgroundImage = global::TetrisPairUI.Properties.Resources.MainBackground;
            this.Wrapper.ColumnCount = 5;
            this.Wrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 155F));
            this.Wrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.Wrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.Wrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.Wrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 155F));
            this.Wrapper.Controls.Add(this.panel2, 4, 1);
            this.Wrapper.Controls.Add(this.flowLayoutPanel2, 3, 0);
            this.Wrapper.Controls.Add(this.flowLayoutPanel3, 0, 0);
            this.Wrapper.Controls.Add(this.flowLayoutPanel1, 2, 1);
            this.Wrapper.Controls.Add(this.panel1, 0, 1);
            this.Wrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Wrapper.ForeColor = System.Drawing.Color.Transparent;
            this.Wrapper.Location = new System.Drawing.Point(0, 0);
            this.Wrapper.Name = "Wrapper";
            this.Wrapper.RowCount = 2;
            this.Wrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.Wrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Wrapper.Size = new System.Drawing.Size(1280, 720);
            this.Wrapper.TabIndex = 1;
            this.Wrapper.Paint += new System.Windows.Forms.PaintEventHandler(this.Wrapper_Paint);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.labelKeyP);
            this.panel2.Controls.Add(this.labelKeyL);
            this.panel2.Controls.Add(this.labelKeyK);
            this.panel2.Controls.Add(this.labelKeyJ);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(1125, 80);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.MinimumSize = new System.Drawing.Size(155, 640);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(155, 640);
            this.panel2.TabIndex = 11;
            // 
            // labelKeyP
            // 
            this.labelKeyP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelKeyP.AutoSize = true;
            this.labelKeyP.Location = new System.Drawing.Point(20, 313);
            this.labelKeyP.Name = "labelKeyP";
            this.labelKeyP.Size = new System.Drawing.Size(100, 25);
            this.labelKeyP.TabIndex = 0;
            this.labelKeyP.Text = "P-поворот";
            this.labelKeyP.Click += new System.EventHandler(this.label2_Click);
            // 
            // labelKeyL
            // 
            this.labelKeyL.AutoSize = true;
            this.labelKeyL.Location = new System.Drawing.Point(20, 270);
            this.labelKeyL.Name = "labelKeyL";
            this.labelKeyL.Size = new System.Drawing.Size(76, 25);
            this.labelKeyL.TabIndex = 0;
            this.labelKeyL.Text = "L-влево";
            // 
            // labelKeyK
            // 
            this.labelKeyK.AutoSize = true;
            this.labelKeyK.Location = new System.Drawing.Point(20, 230);
            this.labelKeyK.Name = "labelKeyK";
            this.labelKeyK.Size = new System.Drawing.Size(67, 25);
            this.labelKeyK.TabIndex = 0;
            this.labelKeyK.Text = "K-вниз";
            // 
            // labelKeyJ
            // 
            this.labelKeyJ.AutoSize = true;
            this.labelKeyJ.Location = new System.Drawing.Point(20, 189);
            this.labelKeyJ.Name = "labelKeyJ";
            this.labelKeyJ.Size = new System.Drawing.Size(86, 25);
            this.labelKeyJ.TabIndex = 0;
            this.labelKeyJ.Text = "J-вправо";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.Wrapper.SetColumnSpan(this.flowLayoutPanel2, 2);
            this.flowLayoutPanel2.Controls.Add(this.buttonBlockPause);
            this.flowLayoutPanel2.Controls.Add(this.labelPlayer2);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(768, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(509, 74);
            this.flowLayoutPanel2.TabIndex = 6;
            this.flowLayoutPanel2.WrapContents = false;
            this.flowLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel2_Paint);
            // 
            // buttonBlockPause
            // 
            this.buttonBlockPause.ColorButton = TetrisPairLibrary.BlockColor.BlockColorList.Green;
            this.buttonBlockPause.ForeColor = System.Drawing.Color.White;
            this.buttonBlockPause.Ledge = 5;
            this.buttonBlockPause.Location = new System.Drawing.Point(309, 20);
            this.buttonBlockPause.Margin = new System.Windows.Forms.Padding(0, 20, 20, 0);
            this.buttonBlockPause.Name = "buttonBlockPause";
            this.buttonBlockPause.PositionText = new System.Drawing.Point(50, 0);
            this.buttonBlockPause.Size = new System.Drawing.Size(180, 50);
            this.buttonBlockPause.TabIndex = 5;
            this.buttonBlockPause.TextButton = "Пауза";
            this.buttonBlockPause.Click += new System.EventHandler(this.buttonBlockPause_Click);
            // 
            // labelPlayer2
            // 
            this.labelPlayer2.AutoSize = true;
            this.labelPlayer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPlayer2.Location = new System.Drawing.Point(181, 20);
            this.labelPlayer2.Margin = new System.Windows.Forms.Padding(0, 20, 20, 0);
            this.labelPlayer2.Name = "labelPlayer2";
            this.labelPlayer2.Size = new System.Drawing.Size(108, 50);
            this.labelPlayer2.TabIndex = 4;
            this.labelPlayer2.Text = "Результат: 0";
            this.labelPlayer2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
            this.Wrapper.SetColumnSpan(this.flowLayoutPanel3, 2);
            this.flowLayoutPanel3.Controls.Add(this.buttonBlockMenu);
            this.flowLayoutPanel3.Controls.Add(this.labelPlayer1);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(515, 80);
            this.flowLayoutPanel3.TabIndex = 7;
            // 
            // buttonBlockMenu
            // 
            this.buttonBlockMenu.ColorButton = TetrisPairLibrary.BlockColor.BlockColorList.Magenta;
            this.buttonBlockMenu.ForeColor = System.Drawing.Color.White;
            this.buttonBlockMenu.Ledge = 5;
            this.buttonBlockMenu.Location = new System.Drawing.Point(20, 20);
            this.buttonBlockMenu.Margin = new System.Windows.Forms.Padding(20, 20, 0, 0);
            this.buttonBlockMenu.Name = "buttonBlockMenu";
            this.buttonBlockMenu.PositionText = new System.Drawing.Point(55, 0);
            this.buttonBlockMenu.Size = new System.Drawing.Size(180, 50);
            this.buttonBlockMenu.TabIndex = 4;
            this.buttonBlockMenu.TextButton = "Меню";
            this.buttonBlockMenu.Click += new System.EventHandler(this.buttonBlockMenu_Click);
            // 
            // labelPlayer1
            // 
            this.labelPlayer1.AutoSize = true;
            this.labelPlayer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPlayer1.Location = new System.Drawing.Point(220, 20);
            this.labelPlayer1.Margin = new System.Windows.Forms.Padding(20, 20, 0, 0);
            this.labelPlayer1.Name = "labelPlayer1";
            this.labelPlayer1.Size = new System.Drawing.Size(108, 50);
            this.labelPlayer1.TabIndex = 3;
            this.labelPlayer1.Text = "Результат: 0";
            this.labelPlayer1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.pictureNextFigure);
            this.flowLayoutPanel1.Controls.Add(this.labelNextFigure);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(518, 83);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(244, 634);
            this.flowLayoutPanel1.TabIndex = 8;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // pictureNextFigure
            // 
            this.pictureNextFigure.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureNextFigure.BackColor = System.Drawing.Color.Transparent;
            this.pictureNextFigure.Location = new System.Drawing.Point(20, 3);
            this.pictureNextFigure.Margin = new System.Windows.Forms.Padding(20, 3, 0, 3);
            this.pictureNextFigure.Name = "pictureNextFigure";
            this.pictureNextFigure.Size = new System.Drawing.Size(200, 200);
            this.pictureNextFigure.TabIndex = 2;
            this.pictureNextFigure.TabStop = false;
            // 
            // labelNextFigure
            // 
            this.labelNextFigure.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelNextFigure.AutoSize = true;
            this.labelNextFigure.Location = new System.Drawing.Point(24, 206);
            this.labelNextFigure.Name = "labelNextFigure";
            this.labelNextFigure.Size = new System.Drawing.Size(171, 25);
            this.labelNextFigure.TabIndex = 3;
            this.labelNextFigure.Text = "Следующая фигура";
            this.labelNextFigure.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.labelKeyR);
            this.panel1.Controls.Add(this.labelKeyD);
            this.panel1.Controls.Add(this.labelKeyS);
            this.panel1.Controls.Add(this.labelKeyA);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 80);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.MinimumSize = new System.Drawing.Size(155, 640);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(155, 640);
            this.panel1.TabIndex = 9;
            // 
            // labelKeyR
            // 
            this.labelKeyR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelKeyR.AutoSize = true;
            this.labelKeyR.Location = new System.Drawing.Point(20, 313);
            this.labelKeyR.Name = "labelKeyR";
            this.labelKeyR.Size = new System.Drawing.Size(101, 25);
            this.labelKeyR.TabIndex = 0;
            this.labelKeyR.Text = "R-поворот";
            this.labelKeyR.Click += new System.EventHandler(this.label1_Click);
            // 
            // labelKeyD
            // 
            this.labelKeyD.AutoSize = true;
            this.labelKeyD.Location = new System.Drawing.Point(20, 270);
            this.labelKeyD.Name = "labelKeyD";
            this.labelKeyD.Size = new System.Drawing.Size(81, 25);
            this.labelKeyD.TabIndex = 0;
            this.labelKeyD.Text = "D-влево";
            this.labelKeyD.Click += new System.EventHandler(this.label1_Click);
            // 
            // labelKeyS
            // 
            this.labelKeyS.AutoSize = true;
            this.labelKeyS.Location = new System.Drawing.Point(20, 230);
            this.labelKeyS.Name = "labelKeyS";
            this.labelKeyS.Size = new System.Drawing.Size(67, 25);
            this.labelKeyS.TabIndex = 0;
            this.labelKeyS.Text = "S-вниз";
            this.labelKeyS.Click += new System.EventHandler(this.label1_Click);
            // 
            // labelKeyA
            // 
            this.labelKeyA.AutoSize = true;
            this.labelKeyA.Location = new System.Drawing.Point(20, 190);
            this.labelKeyA.Name = "labelKeyA";
            this.labelKeyA.Size = new System.Drawing.Size(92, 25);
            this.labelKeyA.TabIndex = 0;
            this.labelKeyA.Text = "A-вправо";
            this.labelKeyA.Click += new System.EventHandler(this.label1_Click);
            // 
            // GameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.Wrapper);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "GameForm";
            this.Text = "Игра";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.GameForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.GameForm_KeyUp);
            this.Wrapper.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureNextFigure)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TableLayoutPanel Wrapper;
        private System.Windows.Forms.PictureBox pictureNextFigure;
        private System.Windows.Forms.Label labelPlayer1;
        private System.Windows.Forms.Label labelPlayer2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label labelNextFigure;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelKeyA;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelKeyP;
        private System.Windows.Forms.Label labelKeyL;
        private System.Windows.Forms.Label labelKeyK;
        private System.Windows.Forms.Label labelKeyJ;
        private System.Windows.Forms.Label labelKeyR;
        private System.Windows.Forms.Label labelKeyD;
        private System.Windows.Forms.Label labelKeyS;
        private ButtonBlock buttonBlockMenu;
        private ButtonBlock buttonBlockPause;
    }
}