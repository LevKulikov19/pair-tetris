﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Text;
using System.Windows.Forms;
using TetrisPairLibrary;

namespace TetrisPairUI
{
    public partial class GameForm : Form
    {
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont,
        IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);
        private PrivateFontCollection fonts = new PrivateFontCollection();
        Font pixelFont;

        TetrisPairLibrary.Application application;
        Game game;
        FieldComponent[] fieldComponent;
        public GameForm(TetrisPairLibrary.Application app)
        {
            InitializeComponent();
            application = app;
        }

        private void GameForm_Load(object sender, EventArgs e)
        {
            byte[] fontData = Properties.Resources.opirus_opik;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, Properties.Resources.opirus_opik.Length);
            AddFontMemResourceEx(fontPtr, (uint)Properties.Resources.opirus_opik.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);

            pixelFont = new Font(fonts.Families[0], 16.0F);
            this.labelNextFigure.Font = new Font(pixelFont.FontFamily, 15);
            this.labelKeyA.Font = new Font(pixelFont.FontFamily, 15);
            this.labelKeyS.Font = new Font(pixelFont.FontFamily, 15);
            this.labelKeyD.Font = new Font(pixelFont.FontFamily, 15);
            this.labelKeyR.Font = new Font(pixelFont.FontFamily, 15);
            this.labelKeyJ.Font = new Font(pixelFont.FontFamily, 15);
            this.labelKeyK.Font = new Font(pixelFont.FontFamily, 15);
            this.labelKeyL.Font = new Font(pixelFont.FontFamily, 15);
            this.labelKeyP.Font = new Font(pixelFont.FontFamily, 15);
            this.buttonBlockMenu.Font = new Font(pixelFont.FontFamily, 20);
            this.buttonBlockPause.Font = new Font(pixelFont.FontFamily, 20);
            this.labelPlayer1.Font = new Font(pixelFont.FontFamily, 15);
            this.labelPlayer2.Font = new Font(pixelFont.FontFamily, 15);

            fieldComponent = new FieldComponent[2];
            game = application.StartNewGame();
            game.Win += Game_Win;
            game.FieldUpdate += Game_FieldUpdate;
            game.PointsUpdate += Game_PointsUpdate;
            fieldComponent[0] = new FieldComponent(game.Fields[0]);
            fieldComponent[1] = new FieldComponent(game.Fields[1]);
            fieldComponent[0].Anchor = AnchorStyles.Top;
            fieldComponent[1].Anchor = AnchorStyles.Top;
            Wrapper.Controls.Add(fieldComponent[0], 1, 1);
            Wrapper.Controls.Add(fieldComponent[1], 3, 1);
            DoubleBuffered = true;

        }

        private void Game_PointsUpdate()
        {
            labelPlayer1.Text = PointsPlayerText(1, game.Points[0]);
            labelPlayer2.Text = PointsPlayerText(2, game.Points[1]);
        }

        private String PointsPlayerText (int numberPalyer, int points)
        {
            return "Результат: " + points;
        }

        private void RedrowingField()
        {
            if (game.IsStop) return;
            foreach (FieldComponent item in fieldComponent)
            {
                item.Redrowing();
            }
        }

        private void RedrowingNextFigure()
        {
            int stroke = 5;
            Graphics graphicsNextFigure = pictureNextFigure.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsNextFigure, this.DisplayRectangle);
            BlocksGrid block = game.NextFigure.Grid;

            Point center = new Point(pictureNextFigure.Width/2, pictureNextFigure.Height/2);
            int q = game.NextFigure.GetTopLeft().X;
            int w = game.NextFigure.GetTopRight().X;
            Point shift = new Point((game.NextFigure.GetTopLeft().X * FieldComponent.BlockSize.Width - (game.NextFigure.GetBottomRight().X+1) * FieldComponent.BlockSize.Width) /2,
                                    (game.NextFigure.GetTopLeft().X * FieldComponent.BlockSize.Height - (game.NextFigure.GetBottomLeft().Y+1) * FieldComponent.BlockSize.Height) / 2);
            center = new Point(center.X + shift.X,center.Y + shift.Y);
            bufferedGraphics.Graphics.Clear(TransparencyKey);
            for (int i = 0; i < block.CountRow; i++)
            {
                for (int j = 0; j < block.CountColumn; j++)
                {

                    Point position = new Point(center.X + FieldComponent.BlockSize.Width * j, center.Y + FieldComponent.BlockSize.Height * i);
                    if (block[i, j].Empty == false)
                    {
                        BlockComponent.DrowBlock(position, block[i, j].Color, FieldComponent.BlockSize ,bufferedGraphics.Graphics);
                    }
                }
            }
            Pen pen = new Pen(Color.Blue,stroke);
            bufferedGraphics.Graphics.DrawRectangle(pen, stroke/2, stroke/2, pictureNextFigure.Width - stroke, pictureNextFigure.Height - stroke);
            bufferedGraphics.Render();
            bufferedGraphics.Render(this.CreateGraphics());
        }

        private void Game_FieldUpdate()
        {
            RedrowingField();
            RedrowingNextFigure();
        }

        private void Game_Win()
        {
            game.Win -= Game_Win;
            game.FieldUpdate -= Game_FieldUpdate;
            game.PointsUpdate -= Game_PointsUpdate;
            ((Меню)this.MdiParent).winForm.PlayerWin = game.PlayerWin;
            ((Меню)this.MdiParent).winForm.point1Player = game.Points[0];
            ((Меню)this.MdiParent).winForm.point2Player = game.Points[1];
            ((Меню)this.MdiParent).winForm.UpdateResult();
            ((Меню)this.MdiParent).ShowWinForm();
        }

        private String WinText (int winPlayer,int points1, int points2)
        {
            String result = String.Empty;
            result += winPlayer + " игрок победил\n";
            result += "1 игрок набрал: " + points1 + "\n";
            result += "2 игрок набрал: " + points2;
            return result;
        }

        private void buttonMenu_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void Wrapper_Paint(object sender, PaintEventArgs e)
        {
            RedrowingNextFigure();
        }

        private void GameForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (!game.IsPause)
            {
                if (e.KeyCode == Keys.Escape) ((Меню)this.MdiParent).ShowMenuForm();
                game.GameForm_KeyDown(e.KeyCode);
                RedrowingField();
            }

        }

        private void buttonPause_Click(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void flowLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelNextFigure_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void buttonBlockMenu_Click(object sender, EventArgs e)
        {
            ((Меню)this.MdiParent).ShowMenuForm();
            game.Pause();
        }

        private void buttonBlockPause_Click(object sender, EventArgs e)
        {
            if (!game.IsPause)
            {
                buttonBlockPause.PositionText = new Point(10, 0);
                buttonBlockPause.TextButton = "Продолжить";
                buttonBlockPause.ColorButton = BlockColor.BlockColorList.Red;
                fieldComponent[0].LowSaturaionBlock = true;
                fieldComponent[1].LowSaturaionBlock = true;
                fieldComponent[0].Redrowing();
                fieldComponent[1].Redrowing();
                game.Pause();
            }
            else
            {
                buttonBlockPause.PositionText = new Point(50, 0);
                buttonBlockPause.TextButton = "Пауза";
                buttonBlockPause.ColorButton = BlockColor.BlockColorList.Green;
                fieldComponent[0].LowSaturaionBlock = false;
                fieldComponent[1].LowSaturaionBlock = false;
                fieldComponent[0].Redrowing();
                fieldComponent[1].Redrowing();
                game.Resume();
            }
        }
    }
}
