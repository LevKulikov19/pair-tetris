﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TetrisPairLibrary;

namespace TetrisPairUI
{
    public partial class Меню : Form
    {
        public Form ActiveForm;
        public GameForm gameForm;
        public MenuForm menuForm;
        public SettingsForm settingsForm;
        public СhoiceDifficultyLevelsForm choiceDifficultyLevelsForm;
        public WinForm winForm;
        public AboutGameForm aboutGameForm;

        TetrisPairLibrary.Application application;

        public bool isGameContinueGame
        {
            get
            {
                if (application.Game != null) return true;
                return false;
            }
        }

        public Меню()
        {
            InitializeComponent();
            application = new TetrisPairLibrary.Application();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            gameForm = new GameForm(application);
            menuForm = new MenuForm();
            settingsForm = new SettingsForm(application.Settings);
            choiceDifficultyLevelsForm = new СhoiceDifficultyLevelsForm(application.Settings.GameSettings);
            winForm = new WinForm();
            aboutGameForm = new AboutGameForm();

            ActiveForm = new MenuForm();
            ActiveForm.MdiParent = this;
            ActiveForm.Show();
            Size = new Size(this.Size.Width, this.Size.Height - 1);
        }

        public void ShowGameForm()
        {
            ActiveForm.Hide();
            //ActiveForm.Dispose();
            ActiveForm = gameForm;
            application.Game.Resume();
            ActiveForm.MdiParent = this;
            ActiveForm.Show();
            Update();
            Size = new Size(this.Size.Width, this.Size.Height - 1);
        }

        public void ShowWinForm()
        {
            ActiveForm.Hide();
            gameForm.Dispose();
            application.StopGame();
            gameForm = new GameForm(application);
            ActiveForm = winForm;
            ActiveForm.MdiParent = this;
            ActiveForm.Show();
            Update();
            Size = new Size(this.Size.Width, this.Size.Height - 1);
        }

        public void ShowСhoiceDifficultyLevelsForm()
        {
            ActiveForm.Hide();
            //ActiveForm.Dispose();
            ActiveForm = choiceDifficultyLevelsForm;
            ActiveForm.MdiParent = this;
            ActiveForm.Show();
            Update();
            Size = new Size(this.Size.Width, this.Size.Height - 1);
        }

        public void ShowNewGameForm()
        {
            ActiveForm.Hide();
            gameForm.Dispose();
            gameForm = new GameForm(application);
            ActiveForm = gameForm;
            ActiveForm.MdiParent = this;
            ActiveForm.Show();
            Update();
            Size = new Size(this.Size.Width, this.Size.Height - 1);
        }

        public void ShowMenuForm()
        {
            ActiveForm.Hide();
            //ActiveForm.Dispose();
            ActiveForm = menuForm;
            menuForm.buttonBlockContinueGame.Enabled = isGameContinueGame;

            ActiveForm.MdiParent = this;
            ActiveForm.Show();
            
            Update();
            Size = new Size(this.Size.Width, this.Size.Height - 1);
        }

        public void ShowSettingsForm()
        {
            ActiveForm.Hide();
            //ActiveForm.Dispose();
            ActiveForm = settingsForm;
            ActiveForm.MdiParent = this;
            ActiveForm.Show();
            Update();
            Size = new Size(this.Size.Width, this.Size.Height - 1);
        }

        public void ShowAboutGameForm()
        {
            ActiveForm.Hide();
            //ActiveForm.Dispose();
            ActiveForm = aboutGameForm;
            ActiveForm.MdiParent = this;
            ActiveForm.Show();
            Update();
            Size = new Size(this.Size.Width, this.Size.Height - 1);
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}
