﻿
namespace TetrisPairUI
{
    partial class MenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelGameName = new System.Windows.Forms.Label();
            this.buttonBlockExit = new TetrisPairUI.ButtonBlock();
            this.buttonBlockAboutGame = new TetrisPairUI.ButtonBlock();
            this.buttonBlockContinueGame = new TetrisPairUI.ButtonBlock();
            this.buttonBlockStartGame = new TetrisPairUI.ButtonBlock();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 550F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.labelGameName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonBlockExit, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.buttonBlockAboutGame, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.buttonBlockContinueGame, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.buttonBlockStartGame, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1280, 720);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // labelGameName
            // 
            this.labelGameName.AutoSize = true;
            this.labelGameName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelGameName.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelGameName.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelGameName.Location = new System.Drawing.Point(368, 0);
            this.labelGameName.Name = "labelGameName";
            this.labelGameName.Size = new System.Drawing.Size(544, 180);
            this.labelGameName.TabIndex = 6;
            this.labelGameName.Text = "Парный тетрис";
            this.labelGameName.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.labelGameName.Click += new System.EventHandler(this.labelGameName_Click);
            // 
            // buttonBlockExit
            // 
            this.buttonBlockExit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonBlockExit.ColorButton = TetrisPairLibrary.BlockColor.BlockColorList.Orange;
            this.buttonBlockExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonBlockExit.ForeColor = System.Drawing.Color.White;
            this.buttonBlockExit.Ledge = 5;
            this.buttonBlockExit.Location = new System.Drawing.Point(450, 537);
            this.buttonBlockExit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonBlockExit.Name = "buttonBlockExit";
            this.buttonBlockExit.PositionText = new System.Drawing.Point(-1, -1);
            this.buttonBlockExit.Size = new System.Drawing.Size(380, 78);
            this.buttonBlockExit.TabIndex = 13;
            this.buttonBlockExit.TextButton = "Выйти";
            this.buttonBlockExit.Click += new System.EventHandler(this.buttonBlockExit_Click);
            // 
            // buttonBlockAboutGame
            // 
            this.buttonBlockAboutGame.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonBlockAboutGame.ColorButton = TetrisPairLibrary.BlockColor.BlockColorList.Green;
            this.buttonBlockAboutGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonBlockAboutGame.ForeColor = System.Drawing.Color.White;
            this.buttonBlockAboutGame.Ledge = 5;
            this.buttonBlockAboutGame.Location = new System.Drawing.Point(450, 449);
            this.buttonBlockAboutGame.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonBlockAboutGame.Name = "buttonBlockAboutGame";
            this.buttonBlockAboutGame.PositionText = new System.Drawing.Point(-1, -1);
            this.buttonBlockAboutGame.Size = new System.Drawing.Size(380, 78);
            this.buttonBlockAboutGame.TabIndex = 12;
            this.buttonBlockAboutGame.TextButton = "Об игре";
            this.buttonBlockAboutGame.Click += new System.EventHandler(this.buttonBlockAboutGame_Click);
            // 
            // buttonBlockContinueGame
            // 
            this.buttonBlockContinueGame.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonBlockContinueGame.ColorButton = TetrisPairLibrary.BlockColor.BlockColorList.Blue;
            this.buttonBlockContinueGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonBlockContinueGame.ForeColor = System.Drawing.Color.White;
            this.buttonBlockContinueGame.Ledge = 5;
            this.buttonBlockContinueGame.Location = new System.Drawing.Point(450, 361);
            this.buttonBlockContinueGame.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonBlockContinueGame.Name = "buttonBlockContinueGame";
            this.buttonBlockContinueGame.PositionText = new System.Drawing.Point(-1, -1);
            this.buttonBlockContinueGame.Size = new System.Drawing.Size(380, 78);
            this.buttonBlockContinueGame.TabIndex = 10;
            this.buttonBlockContinueGame.TextButton = "Продолжить игру";
            this.buttonBlockContinueGame.Click += new System.EventHandler(this.buttonBlockContinueGame_Click);
            // 
            // buttonBlockStartGame
            // 
            this.buttonBlockStartGame.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonBlockStartGame.ColorButton = TetrisPairLibrary.BlockColor.BlockColorList.Red;
            this.buttonBlockStartGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonBlockStartGame.ForeColor = System.Drawing.Color.White;
            this.buttonBlockStartGame.Ledge = 5;
            this.buttonBlockStartGame.Location = new System.Drawing.Point(450, 273);
            this.buttonBlockStartGame.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonBlockStartGame.Name = "buttonBlockStartGame";
            this.buttonBlockStartGame.PositionText = new System.Drawing.Point(-1, -1);
            this.buttonBlockStartGame.Size = new System.Drawing.Size(380, 78);
            this.buttonBlockStartGame.TabIndex = 9;
            this.buttonBlockStartGame.TextButton = "Начать игру";
            this.buttonBlockStartGame.Click += new System.EventHandler(this.buttonBlockStartGame_Click);
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MenuForm";
            this.Text = "Меню";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MenuForm_Load);
            this.Shown += new System.EventHandler(this.MenuForm_Shown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelGameName;
        private ButtonBlock buttonBlockStartGame;
        public ButtonBlock buttonBlockContinueGame;
        private ButtonBlock buttonBlockAboutGame;
        private ButtonBlock buttonBlockExit;
    }
}