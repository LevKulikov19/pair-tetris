﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Text;
using System.Windows.Forms;

namespace TetrisPairUI
{
    public partial class MenuForm : Form
    {
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont,
        IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);
        private PrivateFontCollection fonts = new PrivateFontCollection();
        Font pixelFont;

        public MenuForm()
        {
            InitializeComponent();

            byte[] fontData = Properties.Resources.opirus_opik;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, Properties.Resources.opirus_opik.Length);
            AddFontMemResourceEx(fontPtr, (uint)Properties.Resources.opirus_opik.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);

            pixelFont = new Font(fonts.Families[0], 16.0F);

            this.buttonBlockStartGame.Font = new Font(pixelFont.FontFamily, 25);
            this.buttonBlockContinueGame.Font = new Font(pixelFont.FontFamily, 25);
            //this.buttonBlockSettings.Font = new Font(pixelFont.FontFamily, 25);
            this.buttonBlockAboutGame.Font = new Font(pixelFont.FontFamily, 25);
            this.buttonBlockExit.Font = new Font(pixelFont.FontFamily, 25);
            this.labelGameName.Font = new Font(pixelFont.FontFamily, 50);

            InitBackgroundBlocks();
        }
        
        private void InitBackgroundBlocks()
        {
            Graphics PictureBlockGraphics = this.CreateGraphics();
            PictureBlockGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(PictureBlockGraphics, this.DisplayRectangle);
            bufferedGraphics.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            IntiGraphics(bufferedGraphics);
        }

        private void IntiGraphics(BufferedGraphics bufferedGraphics)
        {
            this.buttonBlockStartGame.SetGraphics(bufferedGraphics);
            this.buttonBlockContinueGame.SetGraphics(bufferedGraphics);
            //this.buttonBlockSettings.SetGraphics(bufferedGraphics);
            this.buttonBlockAboutGame.SetGraphics(bufferedGraphics);
            this.buttonBlockExit.SetGraphics(bufferedGraphics); 
        }

        private void MenuForm_Load(object sender, EventArgs e)
        {
            InitBackgroundBlocks();
            buttonBlockContinueGame.Enabled = ((Меню)this.MdiParent).isGameContinueGame;
        }

        private void MenuForm_Shown(object sender, EventArgs e)
        {
            buttonBlockContinueGame.Enabled = ((Меню)this.MdiParent).isGameContinueGame;
        }

        private void buttonBlockStartGame_Click(object sender, EventArgs e)
        {
            ((Меню)this.MdiParent).ShowСhoiceDifficultyLevelsForm();
        }

        private void buttonBlockContinueGame_Click(object sender, EventArgs e)
        {
            ((Меню)this.MdiParent).ShowGameForm();
        }

        private void buttonBlockSettings_Click(object sender, EventArgs e)
        {
            ((Меню)this.MdiParent).ShowSettingsForm();
        }

        private void buttonBlockAboutGame_Click(object sender, EventArgs e)
        {
            ((Меню)this.MdiParent).ShowAboutGameForm();
        }

        private void buttonBlockExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void labelGameName_Click(object sender, EventArgs e)
        {

        }
    }
}
