﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TetrisPairLibrary;

namespace TetrisPairUI
{
    public partial class SettingsForm : Form
    {
        Settings Settings;
        public SettingsForm(Settings settings)
        {
            InitializeComponent();
            Settings = settings;
        }

        private void buttonMenu_Click(object sender, EventArgs e)
        {
            ((Меню)this.MdiParent).ShowMenuForm();
        }

        private void buttonMenu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) ((Меню)this.MdiParent).ShowMenuForm();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            
        }
    }
}
