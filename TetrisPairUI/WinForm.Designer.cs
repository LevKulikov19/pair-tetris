﻿
namespace TetrisPairUI
{
    partial class WinForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelResutl1Player = new System.Windows.Forms.Label();
            this.labelResutl2Player = new System.Windows.Forms.Label();
            this.labelPoint1Player = new System.Windows.Forms.Label();
            this.labelPoint2Player = new System.Windows.Forms.Label();
            this.labelWhoWin = new System.Windows.Forms.Label();
            this.buttonBlockMenu = new TetrisPairUI.ButtonBlock();
            this.SuspendLayout();
            // 
            // labelResutl1Player
            // 
            this.labelResutl1Player.AutoSize = true;
            this.labelResutl1Player.BackColor = System.Drawing.Color.Transparent;
            this.labelResutl1Player.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelResutl1Player.ForeColor = System.Drawing.Color.White;
            this.labelResutl1Player.Location = new System.Drawing.Point(47, 598);
            this.labelResutl1Player.Name = "labelResutl1Player";
            this.labelResutl1Player.Size = new System.Drawing.Size(275, 41);
            this.labelResutl1Player.TabIndex = 0;
            this.labelResutl1Player.Text = "Результат 1 игрока";
            this.labelResutl1Player.Click += new System.EventHandler(this.labelResutl1Player_Click);
            // 
            // labelResutl2Player
            // 
            this.labelResutl2Player.AutoSize = true;
            this.labelResutl2Player.BackColor = System.Drawing.Color.Transparent;
            this.labelResutl2Player.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelResutl2Player.ForeColor = System.Drawing.Color.White;
            this.labelResutl2Player.Location = new System.Drawing.Point(791, 598);
            this.labelResutl2Player.Name = "labelResutl2Player";
            this.labelResutl2Player.Size = new System.Drawing.Size(275, 41);
            this.labelResutl2Player.TabIndex = 0;
            this.labelResutl2Player.Text = "Результат 2 игрока";
            this.labelResutl2Player.Click += new System.EventHandler(this.label2_Click);
            // 
            // labelPoint1Player
            // 
            this.labelPoint1Player.AutoSize = true;
            this.labelPoint1Player.BackColor = System.Drawing.Color.Transparent;
            this.labelPoint1Player.ForeColor = System.Drawing.Color.White;
            this.labelPoint1Player.Location = new System.Drawing.Point(226, 551);
            this.labelPoint1Player.Name = "labelPoint1Player";
            this.labelPoint1Player.Size = new System.Drawing.Size(22, 25);
            this.labelPoint1Player.TabIndex = 0;
            this.labelPoint1Player.Text = "0";
            this.labelPoint1Player.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelPoint1Player.Click += new System.EventHandler(this.labelResutl1Player_Click);
            // 
            // labelPoint2Player
            // 
            this.labelPoint2Player.AutoSize = true;
            this.labelPoint2Player.BackColor = System.Drawing.Color.Transparent;
            this.labelPoint2Player.ForeColor = System.Drawing.Color.White;
            this.labelPoint2Player.Location = new System.Drawing.Point(965, 551);
            this.labelPoint2Player.Name = "labelPoint2Player";
            this.labelPoint2Player.Size = new System.Drawing.Size(22, 25);
            this.labelPoint2Player.TabIndex = 0;
            this.labelPoint2Player.Text = "0";
            this.labelPoint2Player.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelPoint2Player.Click += new System.EventHandler(this.labelResutl1Player_Click);
            // 
            // labelWhoWin
            // 
            this.labelWhoWin.AutoSize = true;
            this.labelWhoWin.BackColor = System.Drawing.Color.Transparent;
            this.labelWhoWin.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelWhoWin.ForeColor = System.Drawing.Color.White;
            this.labelWhoWin.Location = new System.Drawing.Point(364, 67);
            this.labelWhoWin.Name = "labelWhoWin";
            this.labelWhoWin.Size = new System.Drawing.Size(250, 41);
            this.labelWhoWin.TabIndex = 0;
            this.labelWhoWin.Text = "Победил 1 игрок";
            this.labelWhoWin.Click += new System.EventHandler(this.label2_Click);
            // 
            // buttonBlockMenu
            // 
            this.buttonBlockMenu.ColorButton = TetrisPairLibrary.BlockColor.BlockColorList.Magenta;
            this.buttonBlockMenu.ForeColor = System.Drawing.Color.White;
            this.buttonBlockMenu.Ledge = 5;
            this.buttonBlockMenu.Location = new System.Drawing.Point(47, 29);
            this.buttonBlockMenu.Margin = new System.Windows.Forms.Padding(20, 20, 0, 0);
            this.buttonBlockMenu.Name = "buttonBlockMenu";
            this.buttonBlockMenu.PositionText = new System.Drawing.Point(55, 0);
            this.buttonBlockMenu.Size = new System.Drawing.Size(180, 50);
            this.buttonBlockMenu.TabIndex = 5;
            this.buttonBlockMenu.TextButton = "Меню";
            this.buttonBlockMenu.Click += new System.EventHandler(this.buttonBlockMenu_Click);
            // 
            // WinForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TetrisPairUI.Properties.Resources.BackgroundWin;
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.buttonBlockMenu);
            this.Controls.Add(this.labelWhoWin);
            this.Controls.Add(this.labelResutl2Player);
            this.Controls.Add(this.labelPoint2Player);
            this.Controls.Add(this.labelPoint1Player);
            this.Controls.Add(this.labelResutl1Player);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(1280, 720);
            this.Name = "WinForm";
            this.Text = "Победа";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.WinForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelResutl1Player;
        private System.Windows.Forms.Label labelResutl2Player;
        private System.Windows.Forms.Label labelPoint1Player;
        private System.Windows.Forms.Label labelPoint2Player;
        private System.Windows.Forms.Label labelWhoWin;
        private ButtonBlock buttonBlockMenu;
    }
}