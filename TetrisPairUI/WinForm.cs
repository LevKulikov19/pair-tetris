﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Text;
using System.Windows.Forms;

namespace TetrisPairUI
{
    public partial class WinForm : Form
    {
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont,
        IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);
        private PrivateFontCollection fonts = new PrivateFontCollection();
        Font pixelFont;


        public int PlayerWin
        {
            get;
            set;
        }

        public int point1Player
        {
            get;
            set;
        }

        public int point2Player
        {
            get;
            set;
        }

        public WinForm()
        {
            InitializeComponent();
            PlayerWin = 0;
            point1Player = 0;
            point2Player = 0;

            byte[] fontData = Properties.Resources.opirus_opik;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, Properties.Resources.opirus_opik.Length);
            AddFontMemResourceEx(fontPtr, (uint)Properties.Resources.opirus_opik.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);
            pixelFont = new Font(fonts.Families[0], 16.0F);
            this.labelPoint1Player.Font = new Font(pixelFont.FontFamily, 30);
            this.labelPoint2Player.Font = new Font(pixelFont.FontFamily, 30);
            this.labelResutl1Player.Font = new Font(pixelFont.FontFamily, 30);
            this.labelResutl2Player.Font = new Font(pixelFont.FontFamily, 30);
            this.labelWhoWin.Font = new Font(pixelFont.FontFamily, 45);
            this.buttonBlockMenu.Font = new Font(pixelFont.FontFamily, 20);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void labelResutl1Player_Click(object sender, EventArgs e)
        {

        }

        private void WinForm_Load(object sender, EventArgs e)
        {

        }

        public void UpdateResult ()
        {
            labelPoint1Player.Text = point1Player.ToString();
            labelPoint2Player.Text = point2Player.ToString();
            String s = "Победил " + PlayerWin + " игрок";
            labelWhoWin.Text = s;
        }

        private void buttonBlockMenu_Click(object sender, EventArgs e)
        {
            ((Меню)this.MdiParent).ShowMenuForm();
        }
    }
}
