﻿
namespace TetrisPairUI
{
    partial class СhoiceDifficultyLevelsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(СhoiceDifficultyLevelsForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelСhoiceDifficultyLevels = new System.Windows.Forms.Label();
            this.buttonBlockEasy = new TetrisPairUI.ButtonBlock();
            this.buttonBlockMiddle = new TetrisPairUI.ButtonBlock();
            this.buttonBlockHard = new TetrisPairUI.ButtonBlock();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.25F));
            this.tableLayoutPanel1.Controls.Add(this.labelСhoiceDifficultyLevels, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonBlockEasy, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonBlockMiddle, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.buttonBlockHard, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1280, 720);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // labelСhoiceDifficultyLevels
            // 
            this.labelСhoiceDifficultyLevels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.labelСhoiceDifficultyLevels.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.labelСhoiceDifficultyLevels, 3);
            this.labelСhoiceDifficultyLevels.Font = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelСhoiceDifficultyLevels.ForeColor = System.Drawing.Color.White;
            this.labelСhoiceDifficultyLevels.Location = new System.Drawing.Point(123, 0);
            this.labelСhoiceDifficultyLevels.Name = "labelСhoiceDifficultyLevels";
            this.labelСhoiceDifficultyLevels.Size = new System.Drawing.Size(1033, 200);
            this.labelСhoiceDifficultyLevels.TabIndex = 0;
            this.labelСhoiceDifficultyLevels.Text = "Выберите уровень сложности";
            this.labelСhoiceDifficultyLevels.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonBlockEasy
            // 
            this.buttonBlockEasy.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonBlockEasy.ColorButton = TetrisPairLibrary.BlockColor.BlockColorList.Blue;
            this.buttonBlockEasy.Font = new System.Drawing.Font("Segoe UI", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonBlockEasy.ForeColor = System.Drawing.Color.White;
            this.buttonBlockEasy.Ledge = 5;
            this.buttonBlockEasy.Location = new System.Drawing.Point(450, 250);
            this.buttonBlockEasy.Margin = new System.Windows.Forms.Padding(0);
            this.buttonBlockEasy.Name = "buttonBlockEasy";
            this.buttonBlockEasy.PositionText = new System.Drawing.Point(-1, -1);
            this.buttonBlockEasy.Size = new System.Drawing.Size(380, 80);
            this.buttonBlockEasy.TabIndex = 8;
            this.buttonBlockEasy.TextButton = "Простой";
            this.buttonBlockEasy.Click += new System.EventHandler(this.buttonBlockEasy_Click);
            // 
            // buttonBlockMiddle
            // 
            this.buttonBlockMiddle.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonBlockMiddle.ColorButton = TetrisPairLibrary.BlockColor.BlockColorList.Green;
            this.buttonBlockMiddle.Font = new System.Drawing.Font("Segoe UI", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonBlockMiddle.ForeColor = System.Drawing.Color.White;
            this.buttonBlockMiddle.Ledge = 5;
            this.buttonBlockMiddle.Location = new System.Drawing.Point(450, 380);
            this.buttonBlockMiddle.Margin = new System.Windows.Forms.Padding(0);
            this.buttonBlockMiddle.Name = "buttonBlockMiddle";
            this.buttonBlockMiddle.PositionText = new System.Drawing.Point(-1, -1);
            this.buttonBlockMiddle.Size = new System.Drawing.Size(380, 80);
            this.buttonBlockMiddle.TabIndex = 9;
            this.buttonBlockMiddle.TextButton = "Средний";
            this.buttonBlockMiddle.Click += new System.EventHandler(this.buttonBlockMiddle_Click);
            // 
            // buttonBlockHard
            // 
            this.buttonBlockHard.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonBlockHard.ColorButton = TetrisPairLibrary.BlockColor.BlockColorList.Red;
            this.buttonBlockHard.Font = new System.Drawing.Font("Segoe UI", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonBlockHard.ForeColor = System.Drawing.Color.White;
            this.buttonBlockHard.Ledge = 5;
            this.buttonBlockHard.Location = new System.Drawing.Point(450, 510);
            this.buttonBlockHard.Margin = new System.Windows.Forms.Padding(0);
            this.buttonBlockHard.Name = "buttonBlockHard";
            this.buttonBlockHard.PositionText = new System.Drawing.Point(-1, -1);
            this.buttonBlockHard.Size = new System.Drawing.Size(380, 80);
            this.buttonBlockHard.TabIndex = 10;
            this.buttonBlockHard.TextButton = "Сложный";
            this.buttonBlockHard.Click += new System.EventHandler(this.buttonBlockHard_Click);
            // 
            // СhoiceDifficultyLevelsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "СhoiceDifficultyLevelsForm";
            this.Text = "Выбор уровня сложности";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelСhoiceDifficultyLevels;
        private ButtonBlock buttonBlockEasy;
        private ButtonBlock buttonBlockMiddle;
        private ButtonBlock buttonBlockHard;
    }
}