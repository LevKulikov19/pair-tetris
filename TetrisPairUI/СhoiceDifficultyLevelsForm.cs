﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Text;
using System.Windows.Forms;
using TetrisPairLibrary;

namespace TetrisPairUI
{
    public partial class СhoiceDifficultyLevelsForm : Form
    {
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont,
        IntPtr pdv, [System.Runtime.InteropServices.In] ref uint pcFonts);
        private PrivateFontCollection fonts = new PrivateFontCollection();
        Font pixelFont;

        GameSettings settings;
        public СhoiceDifficultyLevelsForm(GameSettings gameSettings)
        {
            InitializeComponent();
            settings = gameSettings;

            byte[] fontData = Properties.Resources.opirus_opik;
            IntPtr fontPtr = System.Runtime.InteropServices.Marshal.AllocCoTaskMem(fontData.Length);
            System.Runtime.InteropServices.Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            uint dummy = 0;
            fonts.AddMemoryFont(fontPtr, Properties.Resources.opirus_opik.Length);
            AddFontMemResourceEx(fontPtr, (uint)Properties.Resources.opirus_opik.Length, IntPtr.Zero, ref dummy);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(fontPtr);

            pixelFont = new Font(fonts.Families[0], 16.0F);

            this.buttonBlockEasy.Font = new Font(pixelFont.FontFamily, 25);
            this.buttonBlockMiddle.Font = new Font(pixelFont.FontFamily, 25);
            this.buttonBlockHard.Font = new Font(pixelFont.FontFamily, 25);
            this.labelСhoiceDifficultyLevels.Font = new Font(pixelFont.FontFamily, 50);
        }

        private void buttonBlockEasy_Click(object sender, EventArgs e)
        {
            settings.DifficultyLevels = GameRules.DifficultyLevels.Easy;
            ((Меню)this.MdiParent).ShowNewGameForm();
        }

        private void buttonBlockMiddle_Click(object sender, EventArgs e)
        {
            settings.DifficultyLevels = GameRules.DifficultyLevels.Medium;
            ((Меню)this.MdiParent).ShowNewGameForm();
        }

        private void buttonBlockHard_Click(object sender, EventArgs e)
        {
            settings.DifficultyLevels = GameRules.DifficultyLevels.Hard;
            ((Меню)this.MdiParent).ShowNewGameForm();
        }
    }
}
